import { BrowserModule, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { AuthenticationModule } from './core/auth/authentication.module';
import { NgxPermissionsModule } from 'ngx-permissions';

import { LayoutModule } from './content/layout/layout.module';
import { PartialsModule } from './content/partials/partials.module';
import { CoreModule } from './core/core.module';
import { AclService } from './core/services/acl.service';
import { LayoutConfigService } from './core/services/layout-config.service';
import { MenuConfigService } from './core/services/menu-config.service';
import { PageConfigService } from './core/services/page-config.service';
import { UtilsService } from './core/services/utils.service';
import { ClassInitService } from './core/services/class-init.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
	GestureConfig, MatInputModule, MatProgressSpinnerModule, MatPaginatorModule, MatSortModule, MatTableModule,
	MatSelectModule, MatMenuModule, MatProgressBarModule, MatButtonModule, MatCheckboxModule, MatDialogModule, MatTabsModule,
	MatNativeDateModule, MatFormFieldModule, MatCardModule, MatRadioModule, MatIconModule, MatDatepickerModule, MatAutocompleteModule, MatSnackBarModule,
	MatTooltipModule, MAT_DIALOG_DEFAULT_OPTIONS
} from '@angular/material';
//============================


//==============================
import { OverlayModule } from '@angular/cdk/overlay';

import { MessengerService } from './core/services/messenger.service';
import { ClipboardService } from './core/services/clipboard.sevice';

import { PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { LayoutConfigStorageService } from './core/services/layout-config-storage.service';
import { LogsService } from './core/services/logs.service';
import { QuickSearchService } from './core/services/quick-search.service';
import { SubheaderService } from './core/services/layout/subheader.service';
import { HeaderService } from './core/services/layout/header.service';
import { MenuHorizontalService } from './core/services/layout/menu-horizontal.service';
import { MenuAsideService } from './core/services/layout/menu-aside.service';
import { LayoutRefService } from './core/services/layout/layout-ref.service';
import { SplashScreenService } from './core/services/splash-screen.service';
import { DataTableService } from './core/services/datatable.service';

// For data teable
import { LayoutUtilsService } from './core/services/layout/layout-utils.service';
import { HttpUtilsService } from './core/services/http-utils.service';

// Shared
import { ActionNotificationComponent } from './content/pages/components/share/action-natification/action-notification.component';
import { DeleteEntityDialogComponent } from './content/pages/components/share/delete-entity-dialog/delete-entity-dialog.component';
import { FetchEntityDialogComponent } from './content/pages/components/share/fetch-entity-dialog/fetch-entity-dialog.component';
import { UpdateStatusDialogComponent } from './content/pages/components/share/update-status-dialog/update-status-dialog.component';
import { AlertComponent } from './content/pages/components/share/alert/alert.component';
import { ConfirmEntityDialogComponent } from './content/pages/components/share/confirm-entity-dialog/confirm-entity-dialog.component';
import { FailPaymentDialogComponent } from './content/pages/components/share/fail-payment-dialog/fail-payment-dialog.component';
import { UploadSlipDialogComponent } from './content/pages/components/share/upload-slip-dialog/upload-slip-dialog.component';

//import { DashboardComponent } from './content/pages/components/dashboard/dashboard.component';
import 'hammerjs';

// Custom import
import { AppConfig } from './config/app.config';
import { UserService } from './service/user/user.service';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
	// suppressScrollX: true
};

import { NgxSpinnerModule } from 'ngx-spinner';

import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { GoogleChartsModule } from 'angular-google-charts';
import { MultiDatepickerModule } from './content/pages/components/multidatepicker/multidatepicker.module';


@NgModule({
	declarations: [
		AppComponent,
		ActionNotificationComponent,
		DeleteEntityDialogComponent,
		FetchEntityDialogComponent,
		UpdateStatusDialogComponent,
		AlertComponent,
		ConfirmEntityDialogComponent,
		FailPaymentDialogComponent,
		UploadSlipDialogComponent
		//DashboardComponent,
	],
	imports: [
		FormsModule,
		ReactiveFormsModule,
		BrowserAnimationsModule,
		BrowserModule,
		AppRoutingModule,
		HttpClientModule,
		LayoutModule,
		PartialsModule,
		CoreModule,
		OverlayModule,
		AuthenticationModule,
		NgxPermissionsModule.forRoot(),
		NgbModule.forRoot(),
		TranslateModule.forRoot(),
		MatProgressSpinnerModule,
		MatInputModule,
		MatPaginatorModule,
		MatSortModule,
		MatTableModule,
		MatSelectModule,
		MatMenuModule,
		MatProgressBarModule,
		MatButtonModule,
		MatCheckboxModule,
		MatDialogModule,
		MatTabsModule,
		MatNativeDateModule,
		MatCardModule,
		MatRadioModule,
		MatFormFieldModule,
		MatIconModule,
		MatDatepickerModule,
		MatAutocompleteModule,
		MatSnackBarModule,
		MatTooltipModule,
		NgxSpinnerModule,
		NgxMaterialTimepickerModule.forRoot(),
		GoogleChartsModule,
		MultiDatepickerModule
	],
	entryComponents: [
		ActionNotificationComponent,
		DeleteEntityDialogComponent,
		FetchEntityDialogComponent,
		UpdateStatusDialogComponent,
		AlertComponent,
		ConfirmEntityDialogComponent,
		FailPaymentDialogComponent,
		UploadSlipDialogComponent
	],
	providers: [
		UserService,
		AclService,
		LayoutConfigService,
		LayoutConfigStorageService,
		LayoutRefService,
		MenuConfigService,
		PageConfigService,
		UtilsService,
		ClassInitService,
		MessengerService,
		ClipboardService,
		LogsService,
		QuickSearchService,
		DataTableService,
		SplashScreenService,
		{
			provide: PERFECT_SCROLLBAR_CONFIG,
			useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
		},
		{ provide: 'APP_NAME', useValue: AppConfig.APP_NAME },
		{ provide: 'APP_URL', useValue: AppConfig.APP_URL },
		{ provide: 'API_URL', useValue: AppConfig.API_URL },
		{ provide: 'FILEUPLOAD_URL', useValue: AppConfig.FILEUPLOAD_URL },
		{ provide: 'LIMIT_FILE_SIZE', useValue: AppConfig.LIMIT_FILE_SIZE },

		// template services
		SubheaderService,
		HeaderService,
		MenuHorizontalService,
		MenuAsideService,
		{
			provide: HAMMER_GESTURE_CONFIG,
			useClass: GestureConfig
		},
		{
			provide: MAT_DIALOG_DEFAULT_OPTIONS,
			useValue: {
				hasBackdrop: true,
				panelClass: 'm-mat-dialog-container__wrapper',
				height: 'auto',
				width: '900px'
			}
		},
		// Custom service
		UserService,
		HttpUtilsService,
		LayoutUtilsService,
	],
	bootstrap: [AppComponent]
})
export class AppModule {
}
