import {
	Component,
	OnInit,
	Input,
	Output,
	ViewChild,
	ElementRef,
	ChangeDetectorRef
} from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { AuthenticationService } from '../../../../core/auth/authentication.service';
import { NgForm } from '@angular/forms';
import * as objectPath from 'object-path';
import { AuthNoticeService } from '../../../../core/auth/auth-notice.service';
import { SpinnerButtonOptions } from '../../../partials/content/general/spinner-button/button-options.interface';
import { TranslateService } from '@ngx-translate/core';
import { QueryParamsModel } from '../../../../core/models/query-params.model';
import {FormControl, Validators} from '@angular/forms';
import {CardTypeService} from '../../../../service/card_type/card_type.service';
import {applicanttypesService} from '../../../../service/applicant_types/applicant_types.service';
import {TitleService} from '../../../../service/title/title.service';
import {FacultyService} from '../../../../service/faculty/faculty.service';
import {DepartmentService} from '../../../../service/department/department.service';
import {LevelService} from '../../../../service/level/level.service';
import {ProvinceService} from '../../../../service/province/province.service';
import {DistrictService} from '../../../../service/district/district.service';
import {SubDistrictService} from '../../../../service/subdistrict/subdistrict.service';
import {ZipcodeService} from '../../../../service/zipcode/zipcode.service';
import { SelectionModel } from '@angular/cdk/collections';
import { TitleModel } from '../../../../core/models/title.model';
import { HttpClient } from '@angular/common/http';
import { element } from '@angular/core/src/render3/instructions';
@Component({
	selector: 'm-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.scss']
})



export class RegisterComponent implements OnInit {
	public model: any = { email: '' , user_type_id:'2' };
	@Input() action: string;
	@Output() actionChange = new Subject<string>();

	public loading = false;
	titleData = [];
	facultyData=[];
	departmentData=[];
	levelData=[];
	provinceData=[];
	DistrictData=[];
	SubdistrictData=[];
	ZipcodeData=[];
	cardtypesData= [];
	apptypesData= [];


	displayOtherTitle =false;
	displayid=false;
	displaythaiID=false;
	displaypassport=false;
	//displaytextbox=[false,false,false,false];//['thai_id'0,'passport'1,'stu_id'2,'title'3],
	
	
	@ViewChild('f') f: NgForm; 
	errors: any = [];
	test: any=[];
	spinner: SpinnerButtonOptions = {
		active: false,
		spinnerSize: 18,
		raised: true,
		buttonColor: 'primary',
		spinnerColor: 'accent',
		fullWidth: false
	};

	constructor(
		private authService: AuthenticationService,
		public authNoticeService: AuthNoticeService,
		private titleService: TitleService,
		private translate: TranslateService,
		private cdr: ChangeDetectorRef,
		private httpClient: HttpClient,
		
		private cardtypeService: CardTypeService,
		private ApplicanttypesService: applicanttypesService,
		private facultyService: FacultyService,		
		private departmentService: DepartmentService,		
		private levelService: LevelService,
		private provinceService: ProvinceService,
		private districtService: DistrictService,
		private subdistrictService: SubDistrictService,
		private zipcodeService: ZipcodeService,
	) { }

	ngOnInit() {
		this.getcardtype();
		this.getPro();
		this.getfaculty();
	}

	getcardtype(){
		this.cardtypeService.getAll().subscribe(res => {
			this.cardtypesData = res.data;
		})
		this.getapplicationtype();
	}


	showCardfill(card_id){
		this.model.card_no='';

		if(card_id===1)
		{
			this.displaythaiID=true;
			this.displaypassport=false;
			// this.displaytextbox[0]=true;
			// console.log(this.displaytextbox[0]);
		}
		else{
			this.displaythaiID=false;
			this.displaypassport=true;
		}
	
		
	}
	showid(stuid){
		this.model.stu_id='';
		if(stuid===2)
			this.displayid=true;
		else
			this.displayid=false;
	}
	getfaculty(){
		this.facultyService.getAll().subscribe(res => {
			this.facultyData = res.data;		
		})
	}
	getdepart(fid){
		this.departmentService.getDepartment(fid).subscribe(res => {
			this.departmentData = res.data;
		})
	}
	getLevel() {
		this.levelService.getAll().subscribe(res => {
			this.levelData = res.data;
		})
	}
	getPro(){
		this.provinceService.getAll().subscribe(res => {
			this.provinceData = res.data;		
		})
	}
	getDis(pid){

		this.districtService.getDistrict(pid).subscribe(res => {
			this.DistrictData = res.data;
		})
	}

	getSubDis(did){
	
		this.subdistrictService.getSubDistrict(did).subscribe(res => {
			this.SubdistrictData = res.data;
		})
	}

	getZipcode(sid){
		this.zipcodeService.getZip(sid).subscribe(res => {
			this.ZipcodeData = res.data[0].zipcode;
		})
	}

	getTitle() {
		this.titleService.getAll().subscribe(res => {
			this.titleData = res.data;
		})
	}

	showothertitle(tid){
		this.model.other_title='';
		if(tid===7)
			this.displayOtherTitle=true;
		else
			this.displayOtherTitle=false;
	}

	getapplicationtype(){
		this.ApplicanttypesService.getAll().subscribe(res => {
			this.apptypesData = res.data;
		})
		this.getTitle();
		this.getLevel();
	}

	thai_national_id_accuracy_check(thai_ID){
		if(thai_ID.length===13)
		{
			if(thai_ID.slice(0,1)!=0 && thai_ID.slice(0,1)!=9 ){
				let last_digit=0,j=1,k=13;
				for(let i=0; i<12 ; i++)
				{
					last_digit=last_digit+(thai_ID.slice(i,j)*k);
					j++;
					k--;
				};
				last_digit=11-(last_digit%11);
				if(last_digit.toString() === thai_ID.slice(-1)){
					return true;	
				}
			}
		}
		return false;
	}

	loginPage(event: Event) {
		this.authNoticeService.setNotice(null);
		event.preventDefault();
		this.action = 'login';
		this.actionChange.next(this.action);
	}

	submit() {
		this.spinner.active = true;
		
		if (this.validate(this.f)) {
			//console.log(this.validate(this.f)) true or false
			console.log(this.model)
			this.authService.register(this.model).subscribe(response => {
				
				if (response.status === 'success') {
					this.action = 'login';
					this.actionChange.next(this.action);
					this.spinner.active = false;
					this.authNoticeService.setNotice(this.translate.instant('AUTH.REGISTER.SUCCESS'), 'success');
				}
				else{
					this.errors = [];
					if(response.error.card_no){
						if(this.displaythaiID)
							this.errors.push(this.translate.instant('AUTH.VALIDATION.DUPLICATE', { name: this.translate.instant('AUTH.INPUT.NATIONAL_ID') }));
						else
							this.errors.push(this.translate.instant('AUTH.VALIDATION.DUPLICATE', { name: this.translate.instant('AUTH.INPUT.PASSPORT') }));
					}
					if(response.error.email){
						this.errors.push("อีเมลถูกเลือกแล้ว");
					}
					if(response.error.stu_id){
						this.errors.push("รหัสนิสิตถูกเลือกแล้ว");
					}
					this.authNoticeService.setNotice(this.errors.join('<br/>'), 'error');
					this.spinner.active = false;
					this.cdr.detectChanges();
					window.scroll(0,0);

				}
				
			});
		}
	}



	validate(f: NgForm) {
		this.errors = [];

		// if (f.form.status === 'VALID') {
			
		// 	return true;
		// }

		// check password matching
		if (objectPath.get(f, 'form.controls.password.value') != objectPath.get(f, 'form.controls.rpassword.value')) {
			this.errors.push(this.translate.instant('AUTH.VALIDATION.PWD_MATCH', { name: this.translate.instant('AUTH.INPUT.PASSWORD') }));
		}
		// check Thai ID
		if(this.displaythaiID){
			if(!this.thai_national_id_accuracy_check(objectPath.get(f, 'form.controls.thaiID.value'))){
				this.errors.push(this.translate.instant('AUTH.VALIDATION.INVALID', { name: this.translate.instant('AUTH.INPUT.NATIONAL_ID') }));
			}
		}
		if (objectPath.get(f, 'form.controls.firstname.errors.required')) {
			this.errors.push(this.translate.instant('AUTH.VALIDATION.REQUIRED', { name: this.translate.instant('AUTH.INPUT.FIRSTNAME') }));
		}

		if (objectPath.get(f, 'form.controls.lastname.errors.required')) {
			this.errors.push(this.translate.instant('AUTH.VALIDATION.REQUIRED', { name: this.translate.instant('AUTH.INPUT.LASTNAME') }));
		}
		if (objectPath.get(f, 'form.controls.stu_id.errors.required')) {
			this.errors.push(this.translate.instant('AUTH.VALIDATION.REQUIRED', { name: this.translate.instant('AUTH.INPUT.STU_ID') }));
		}
		if (objectPath.get(f, 'form.controls.title.errors.required')) {
			this.errors.push(this.translate.instant('AUTH.VALIDATION.REQUIRED', { name: this.translate.instant('AUTH.INPUT.TITLE') }));
		}

		if (objectPath.get(f, 'form.controls.Province.errors.required')) {
			this.errors.push(this.translate.instant('AUTH.VALIDATION.REQUIRED', { name: this.translate.instant('AUTH.INPUT.FACULTY') }));
		}

		if (objectPath.get(f, 'form.controls.Distirct.errors.required')) {
			this.errors.push(this.translate.instant('AUTH.VALIDATION.REQUIRED', { name: this.translate.instant('AUTH.INPUT.DEPARTMENT') }));
		}

		if (objectPath.get(f, 'form.controls.Sub_Distirct.errors.required')) {
			this.errors.push(this.translate.instant('AUTH.VALIDATION.REQUIRED', { name: this.translate.instant('AUTH.INPUT.LEVEL') }));
		}

		if (objectPath.get(f, 'form.controls.Telephone.errors.required' )) {
			this.errors.push(this.translate.instant('AUTH.VALIDATION.REQUIRED', { name: this.translate.instant('AUTH.INPUT.TELEPHONE') }));
		}
		if (objectPath.get(f, 'form.controls.email.errors.required')) {
			this.errors.push(this.translate.instant('AUTH.VALIDATION.REQUIRED', { name: this.translate.instant('AUTH.INPUT.EMAIL') }));
		}

		if (objectPath.get(f, 'form.controls.Province.errors.required')) {
			this.errors.push(this.translate.instant('AUTH.VALIDATION.REQUIRED', { name: this.translate.instant('AUTH.INPUT.PROVINCE') }));
		}

		if (objectPath.get(f, 'form.controls.Distirct.errors.required')) {
			this.errors.push(this.translate.instant('AUTH.VALIDATION.REQUIRED', { name: this.translate.instant('AUTH.INPUT.DISTRICT') }));
		}

		if (objectPath.get(f, 'form.controls.Sub_Distirct.errors.required')) {
			this.errors.push(this.translate.instant('AUTH.VALIDATION.REQUIRED', { name: this.translate.instant('AUTH.INPUT.SUBDISTRICT') }));
		}

		if (objectPath.get(f, 'form.controls.ZipCode.errors.required')) {
			this.errors.push(this.translate.instant('AUTH.VALIDATION.REQUIRED', { name: this.translate.instant('AUTH.INPUT.ZIPCODE') }));
		}


		if (objectPath.get(f, 'form.controls.password.errors.required')) {
			this.errors.push(this.translate.instant('AUTH.VALIDATION.REQUIRED', { name: this.translate.instant('AUTH.INPUT.PASSWORD') }));
		}
		if (objectPath.get(f, 'form.controls.password.errors.minlength')) {
			this.errors.push(this.translate.instant('AUTH.VALIDATION.MIN_LENGTH', { name: this.translate.instant('AUTH.INPUT.PASSWORD'), min: 4 }));
		}

		if (objectPath.get(f, 'form.controls.rpassword.errors.required')) {
			this.errors.push(this.translate.instant('AUTH.VALIDATION.REQUIRED', { name: this.translate.instant('AUTH.INPUT.CONFIRM_PASSWORD') }));
		}
		

		if (this.errors.length > 0) {
			this.authNoticeService.setNotice(this.errors.join('<br/>'), 'error');
			this.spinner.active = false;
			window.scroll(0,0);
		}else{
			return true;
		}
		return false;
	}

}
