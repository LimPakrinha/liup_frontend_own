import {
	Component,
	OnInit,
	Output,
	Input,
	ViewChild,
	OnDestroy,
	ChangeDetectionStrategy,
	ChangeDetectorRef,
	HostBinding
} from '@angular/core';
import { AuthenticationService } from '../../../../core/auth/authentication.service';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { AuthNoticeService } from '../../../../core/auth/auth-notice.service';
import { NgForm } from '@angular/forms';
import * as objectPath from 'object-path';
import { TranslateService } from '@ngx-translate/core';
import { SpinnerButtonOptions } from '../../../partials/content/general/spinner-button/button-options.interface';
import { UserService } from '../../../../service/user/user.service';

@Component({
	selector: 'm-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent implements OnInit, OnDestroy {
	public model: any = { card_no: '', password: '' };

	@HostBinding('class') classes: string = 'm-login__signin';
	@Output() actionChange = new Subject<string>();
	public loading = false;

	@Input() action: string;

	@ViewChild('f') f: NgForm;
	errors: any = [];

	spinner: SpinnerButtonOptions = {
		active: false,
		spinnerSize: 18,
		raised: true,
		buttonColor: 'primary',
		spinnerColor: 'accent',
		fullWidth: false
	};

	constructor(
		private authService: AuthenticationService,
		private router: Router,
		public authNoticeService: AuthNoticeService,
		private translate: TranslateService,
		private cdr: ChangeDetectorRef,
		private userService: UserService
	) { }

	submit() {
		this.spinner.active = true;
		if (this.validate(this.f)) {
			this.authService.login(this.model).subscribe(response => {
				if (typeof response !== 'undefined') {
					if (response.status === 'success') {
						if(response.data.user_type_id===1){
							this.router.navigate(['/exam-data']);
						}
						else{
							this.router.navigate(['/user-exam-register']);
						}
					} else {
						//this.authNoticeService.setNotice(response.error, 'error');
						this.authNoticeService.setNotice('หมายเลขบัตรหรือรหัสผ่านของคุณไม่ถูกต้อง','error');
					}
				} else {
					
					//this.authNoticeService.setNotice(this.translate.instant('AUTH.VALIDATION.INVALID_LOGIN',{name: this.translate.instant('AUTH.')}), 'error');
					this.authNoticeService.setNotice('หมายเลขบัตรหรือรหัสผ่านของคุณไม่ถูกต้อง','error');
					
				}
				this.spinner.active = false;
				this.cdr.detectChanges();
			});
		}
	}

	ngOnInit(): void {
		if (!this.authNoticeService.onNoticeChanged$.getValue()) {
			this.authNoticeService.setNotice(null);
		}
	}

	ngOnDestroy(): void {
		this.authNoticeService.setNotice(null);
	}

	validate(f: NgForm) {
		if (f.form.status === 'VALID') {
			return true;
		}

		this.errors = [];

		if (objectPath.get(f, 'form.controls.card_no.errors.required')) {
			// this.errors.push(this.translate.instant('AUTH.VALIDATION.REQUIRED', { name: 'Username' }));
			this.errors.push('กรุณาระบุชื่อบัญชีผู้ใช้ของคุณ');
		}

		if (objectPath.get(f, 'form.controls.password.errors.required')) {
			// this.errors.push(this.translate.instant('AUTH.VALIDATION.INVALID', { name: 'Password' }));
			this.errors.push('กรุณาระบุรหัสผ่านของคุณ');
		}

		if (objectPath.get(f, 'form.controls.password.errors.minlength')) {
			// this.errors.push(this.translate.instant('AUTH.VALIDATION.MIN_LENGTH', { name: 'Password' }));
			this.errors.push('รหัสผ่านของคุณต้องมีความยาวมากกว่า 4');
		}

		if (this.errors.length > 0) {
			this.authNoticeService.setNotice(this.errors.join('<br/>'), 'error');
			this.spinner.active = false;
		}

		return false;
	}

	forgotPasswordPage(event: Event) {
		this.action = 'forgot-password';
		this.actionChange.next(this.action);
	}

	register(event: Event) {
		this.action = 'register';
		this.actionChange.next(this.action);
	}
}
