import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamScoreAdminListComponent } from './exam-score-admin-list.component';

describe('ExamScoreComponent', () => {
  let component: ExamScoreAdminListComponent;
  let fixture: ComponentFixture<ExamScoreAdminListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamScoreAdminListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamScoreAdminListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
