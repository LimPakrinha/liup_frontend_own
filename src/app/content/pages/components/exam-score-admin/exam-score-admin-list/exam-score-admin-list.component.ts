import { ChangeDetectionStrategy, Component, OnInit, ViewChild, ElementRef, HostListener, ChangeDetectorRef, Output, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SubheaderService } from '../../../../../core/services/layout/subheader.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { UserExamService } from '../../../../../service/user-exam/user-exam.service';
import { Location } from '@angular/common';
import { fromEvent, merge } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { QueryParamsModel } from '../../../../../core/models/query-params.model';
import { UserExamDataSource } from '../../../../../core/data-sources/user-exam.datasource';
import { UserExamModel } from '../../../../../core/models/user-exam.model';
import { SelectionModel } from '@angular/cdk/collections';

@Component({
	selector: 'm-exam-score-admin-list',
	templateUrl: './exam-score-admin-list.component.html',
	styleUrls: ['./exam-score-admin-list.component.scss']
})
export class ExamScoreAdminListComponent implements OnInit {

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;

	dataSource: UserExamDataSource;

	// Filter fields
	@ViewChild('searchInput') searchInput: ElementRef;
	filterStatus: string = '';
	filterCondition: string = '';
	// Selection
	selection = new SelectionModel<UserExamModel>(true, []);
	dataResult: UserExamModel[] = [];
	public config: any;
	displayedColumns = ['user_exam_first_name', 'user_exam_last_name', 'user_exam_card_no', 'seat_no', 'score', 'is_pass', 'action']
	private examID: number;
	constructor(
		private _location: Location,
		private router: Router,
		private subheaderService: SubheaderService,
		private cdr: ChangeDetectorRef,
		private SubheaderService: SubheaderService,
		private UserExamService: UserExamService,
		private activatedRoute: ActivatedRoute,
		private route: ActivatedRoute,
		@Inject('API_URL') public API_URL: string,
	) {
	}


	ngOnInit() {
		// this.list()
		this.initSubHeader()
		this.getComponentTitle()
		this.activatedRoute.queryParams.subscribe(params => {
			this.examID = + params.id;

		})
		// If the user changes the sort order, reset back to the first page.
		this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

		/* Data load will be triggered in two cases:
		- when a pagination event occurs => this.paginator.page
		- when a sort event occurs => this.sort.sortChange
		**/
		merge(this.sort.sortChange, this.paginator.page)
			.pipe(
				tap(() => {
					this.loadList();
				})
			)
			.subscribe();

		// Filtration, bind to searchInput
		fromEvent(this.searchInput.nativeElement, 'keyup')
			.pipe(
				debounceTime(150),
				distinctUntilChanged(),
				tap(() => {
					this.paginator.pageIndex = 0;
					this.loadList();
				})
			)
			.subscribe();

		// Set title to page breadCrumbs
		// Init DataSource
		this.dataSource = new UserExamDataSource(this.UserExamService);

		let queryParams = new QueryParamsModel({});
		// Read from URL itemId, for restore previous state
		this.route.queryParams.subscribe(params => {
			if (params.id) {
				// queryParams = this.userService.lastFilter$.getValue();
				// this.restoreState(queryParams, +params.id);
			}
			// First load
			this.dataSource.loadExamScoreData(this.examID, queryParams);
			console.log(this.dataSource)
		});
		this.dataSource.entitySubject.subscribe(res => this.dataResult = res);

	}

	goBack() {
		this._location.back();
	}
	updatePage(data) {
		// console.log(data)
		this.router.navigate(['/exam-score-admin', data]);
	}
	initSubHeader() {
		this.subheaderService.setTitle('ตารางคะแนนสอบ');
		this.subheaderService.setBreadcrumbs([
			{ title: 'ตารางคะแนนของรายการสอบ' }, { title: 'ตารางคะแนนสอบ' }
		]);
	}

	getComponentTitle() {
		let result = `ตารางคะแนนสอบ`;
		return result;
	}
  /** FILTRATION */
  filterConfiguration(): any {
    const filter: any = {};
    const searchText: string = this.searchInput.nativeElement.value;
    filter['user.first_name'] = searchText;
    filter['user.last_name'] = searchText;
    return filter;
  }

  /** SELECTION */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataResult.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
    } else {
      this.dataResult.forEach(row => this.selection.select(row));
    }
  }
  loadList() {
    this.selection.clear();
    const queryParams = new QueryParamsModel(
      this.filterConfiguration(),
      this.sort.direction,
      this.sort.active,
      this.paginator.pageIndex,
      this.paginator.pageSize
    );
    this.dataSource.loadExamScoreData(this.examID, queryParams);
  }
}
