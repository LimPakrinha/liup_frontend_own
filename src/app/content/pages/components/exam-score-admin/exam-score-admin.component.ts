import { ChangeDetectionStrategy, Component, OnInit, ViewChild, ElementRef, HostListener, ChangeDetectorRef, Output, Inject } from '@angular/core';
import { Router } from '@angular/router';
//nimport { examDataSource } from '../../../../core/data-sources/exam.datasource';
import { SubheaderService } from '../../../../core/services/layout/subheader.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { examService } from '../../../../service/exam/exam.service';
import { ExamModel } from '../../../../core/models/exam.model';
import { SelectionModel } from '@angular/cdk/collections';
import { fromEvent, merge } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { DashboardDataSource } from '../../../../core/data-sources/dashboard.datasource';
import { QueryParamsModel } from '../../../../core/models/query-params.model';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'm-exam-score-admin',
	templateUrl: './exam-score-admin.component.html',
	styleUrls: ['./exam-score-admin.component.scss']
})
export class ExamScoreAdminComponent implements OnInit {
	dataSource: DashboardDataSource;

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;

	// Filter fields
	@ViewChild('searchInput') searchInput: ElementRef;
	filterStatus: string = '';
	filterCondition: string = '';
	// Selection
	selection = new SelectionModel<ExamModel>(true, []);
	dataResult: ExamModel[] = [];

	public config: any;
	displayedColumns = ['name', 'exam_date','exam_time','action']
	constructor(
		private router: Router,
		private subheaderService: SubheaderService,
		private route: ActivatedRoute,
		private cdr: ChangeDetectorRef,
		private SubheaderService: SubheaderService,
		private examService: examService,

		@Inject('API_URL') public API_URL: string,
	) {
	}


	ngOnInit() {
		// this.list()
		this.initSubHeader()
		this.getComponentTitle()
		// If the user changes the sort order, reset back to the first page.
		this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

		/* Data load will be triggered in two cases:
		- when a pagination event occurs => this.paginator.page
		- when a sort event occurs => this.sort.sortChange
		**/
		merge(this.sort.sortChange, this.paginator.page)
			.pipe(
				tap(() => {
					this.loadExamList();
				})
			)
			.subscribe();

		// Filtration, bind to searchInput
		fromEvent(this.searchInput.nativeElement, 'keyup')
			.pipe(
				debounceTime(150),
				distinctUntilChanged(),
				tap(() => {
					this.paginator.pageIndex = 0;
					this.loadExamList();
				})
			)
			.subscribe();

		// Set title to page breadCrumbs
		// Init DataSource
		this.dataSource = new DashboardDataSource(this.examService);
		console.log(this.dataSource)
		let queryParams = new QueryParamsModel({});
		// Read from URL itemId, for restore previous state
		this.route.queryParams.subscribe(params => {
			if (params.id) {
				// queryParams = this.userService.lastFilter$.getValue();
				// this.restoreState(queryParams, +params.id);
			}
			// First load
			this.dataSource.loadData(queryParams);
		});
		this.dataSource.entitySubject.subscribe(res => this.dataResult = res);
	}

	// list() {
	// 	this.examService.list().then((res: any) => {
	// 		this.dataSource.data = res.data;
	// 		for (let i= 0; i < this.dataSource.data.length ; i++){
	// 			this.dataSource.data[i].exam_time = res.data[i].exam_start_time.substr(0,5) + " - " + res.data[i].exam_end_time.substr(0,5);
	// 		}
	// 		console.log(this.dataSource.data);
	// 		if (this.dataSource.data.length == 0) {
	// 			this.dataSource.data.push({
	// 				'name': '-',
	// 				'exam_date': '-',
	// 				'exam_time': '-',
	// 				'action': '-',
	// 			})
	// 		}
	// 		this.initFormList();
	// 	})
	// 		.catch((error) => {
	// 			console.log(error);
	// 		})
	// }

	updatePage(data) {
		// console.log(data)
		this.router.navigate(['/exam-score-admin', data]);
	}
	initSubHeader() {
		this.subheaderService.setTitle('ตารางคะแนนสอบ');
		this.subheaderService.setBreadcrumbs([
			{ title: 'ตารางคะแนนของรายการสอบ' }
		]);
	}

	getComponentTitle() {
		let result = `ตารางคะแนนของรายการสอบ`;
		return result;
	}
	/** FILTRATION */
	filterConfiguration(): any {
		const filter: any = {};
		const searchText: string = this.searchInput.nativeElement.value;
		filter['exam.name'] = searchText;
		return filter;
	}

	/** SELECTION */
	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataResult.length;
		return numSelected === numRows;
	}

	/** Selects all rows if they are not all selected; otherwise clear selection. */
	masterToggle() {
		if (this.isAllSelected()) {
			this.selection.clear();
		} else {
			this.dataResult.forEach(row => this.selection.select(row));
		}
	}
	loadExamList() {
		this.selection.clear();
		const queryParams = new QueryParamsModel(
			this.filterConfiguration(),
			this.sort.direction,
			this.sort.active,
			this.paginator.pageIndex,
			this.paginator.pageSize
		);
		this.dataSource.loadData(queryParams);
	}
}
