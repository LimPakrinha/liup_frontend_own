import { Component, OnInit, ChangeDetectorRef, Inject, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SubheaderService } from '../../../../../core/services/layout/subheader.service';
import { LayoutUtilsService, MessageType } from '../../../../../core/services/layout/layout-utils.service';
import { BehaviorSubject } from 'rxjs';
import { DateAdapter } from '@angular/material/core';
import { UserExamModel } from '../../../../../core/models/user-exam.model';
import { UserExamService } from '../../../../../service/user-exam/user-exam.service';
import { status_seatService } from '../../../../../service/status_seat/status_seat.service';
import * as moment from 'moment';
import { Location } from '@angular/common';

@Component({
	selector: 'm-exam-score-admin-form',
	templateUrl: './exam-score-admin-form.component.html',
	styleUrls: ['./exam-score-admin-form.component.scss']
})
export class ExamScoreAdminFormComponent implements OnInit {
	// userExamformModel: UserExamModel;
	UserExamScore: UserExamScore;
	loadingSubject = new BehaviorSubject<boolean>(false);
	loading$ = this.loadingSubject.asObservable();
	UserExamformGroup: FormGroup;
	StatusSeatData: any = [];
	format = 'YYYY/MM/DD';
	displayedColumns = ['id', 'user_exam_first_name', 'user_exam_last_name', 'user_exam_card_no', 'seat_no']

	constructor(
		@Inject('API_URL') private API_URL: string,
		private _location: Location,
		private router: Router,
		private activatedRoute: ActivatedRoute,
		private subheaderService: SubheaderService,
		private formBuilder: FormBuilder,
		private changeDetectorRef: ChangeDetectorRef,
		private userExamService: UserExamService,
		private StatusSeatService: status_seatService,
		private layoutUtilsService: LayoutUtilsService,
	) { }

	ngOnInit() {
		this.createForm();
		this.loadingSubject.next(true);
		// this.getSeat_status();
		this.activatedRoute.queryParams.subscribe(params => {
			const id = + params.id;

			if (id && id > 0) {
				this.userExamService.getExamScoreDetail(id).toPromise().then(res => {
					this.UserExamScore = res.data;
					if(this.UserExamScore.score == 0){
						this.UserExamScore.is_pass = 2;
					}
					if(this.UserExamScore.seat_no == null){
						this.UserExamScore.seat_no = '-';
					}
					this.initPage();
					this.initSubHeader()
				});
			} else {
				const newModel = new UserExamScore();
				newModel.clear();
				this.UserExamScore = newModel;
				this.initSubHeader()
			}
		});
	}
	goBack() {
		this._location.back();
	}
	initPage() {
		this.createForm();
		this.loadingSubject.next(false);
	}

	initSubHeader() {
		this.subheaderService.setTitle('ตารางคะแนนสอบ');
		this.subheaderService.setBreadcrumbs([
			{ title: 'ตารางคะแนนของรายการสอบ'},
			{ title: 'ตารางคะแนนสอบ'},
			{ title: 'แก้ไขคะแนนสอบ' }
		]);
		this.getComponentTitle();
	}

	getComponentTitle() {
		let result = `คะแนนสอบของ : ` + this.UserExamformGroup.value.user_exam_first_name + ' ' + this.UserExamformGroup.value.user_exam_last_name;
		return result;
	}
	createForm() {
		if (this.UserExamScore) {
			this.UserExamformGroup = this.formBuilder.group({
				user_id: [this.UserExamScore.user_id, [Validators.required, Validators.maxLength(255)]],
				exam_id: [this.UserExamScore.exam_id, [Validators.required, Validators.maxLength(255)]],
				// receipt_file: [this.UserExamScore.receipt_file, [Validators.required]],
				seat_no: [this.UserExamScore.seat_no, [Validators.required]],
				score: [this.UserExamScore.score, [Validators.required, Validators.pattern('[0-9]*')]],
				is_reg_confirmed: [this.UserExamScore.is_reg_confirmed, [Validators.required]],
				is_pass: [this.UserExamScore.is_pass, [Validators.required]],
				user_exam_first_name: [this.UserExamScore.user_exam_first_name, [Validators.required]],
				user_exam_last_name: [this.UserExamScore.user_exam_last_name, [Validators.required]],
				user_exam_card_no: [this.UserExamScore.user_exam_card_no, [Validators.required]],
				card_type_id: [this.UserExamScore.card_type_id, [Validators.required]],
			});

			this.changeDetectorRef.detectChanges();

		} else {
			this.UserExamformGroup = this.formBuilder.group({
				user_id: ["", [Validators.required, Validators.pattern('[0-9]*')]],
				exam_id: ["", [Validators.required, Validators.pattern('[0-9]*')]],
				// receipt_file: ["", [Validators.required, Validators.maxLength(255)]],
				seat_no: ["", [Validators.required, Validators.maxLength(255)]],
				score: ["", [Validators.required, Validators.pattern('[0-9]*')]],
				is_reg_confirmed: ["", [Validators.required]],
				is_pass: ["", [Validators.required]],
				user_exam_first_name: ["", [Validators.required, Validators.maxLength(255)]],
				user_exam_last_name: ["", [Validators.required, Validators.maxLength(255)]],
				user_exam_card_no: ["", [Validators.required, Validators.maxLength(255)]],
				card_type_id: ["", [Validators.required, Validators.maxLength(255)]],
			});
		}
		console.log(this.UserExamformGroup.value.card_type_id)

	}
	onSumbit(withBack: boolean = false) {

		const controls = this.UserExamformGroup.controls;
		console.log(this.UserExamformGroup.invalid)
		if (this.UserExamformGroup.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}
		// tslint:disable-next-line:prefer-const
		let editedData = this.prepareUser();
		if (editedData.id > 0) {
			// console.log("go update");
			
			this.update(editedData, withBack);

			return;
		}
		this.add(editedData, withBack);
	}

	prepareUser(): UserExamModel {
		const controls = this.UserExamformGroup.controls;
		const _formValue = new UserExamModel();

		console.log(_formValue, "_formValue")

		_formValue.id = this.UserExamScore.id;
		_formValue.user_id = this.UserExamScore.user_id;
		_formValue.exam_id = this.UserExamScore.exam_id;
		_formValue.receipt_file = this.UserExamScore.receipt_file;
		_formValue.seat_no = this.UserExamScore.seat_no;
		_formValue.is_reg_confirmed = this.UserExamScore.is_reg_confirmed;
		_formValue.score = controls['score'].value;
		_formValue.is_pass = controls['is_pass'].value;
		return _formValue;
	}

	update(_formValue: UserExamModel, withBack: boolean = false) {
		// console.log("update");
		
		this.userExamService.update(_formValue.id, _formValue)
			.then((res) => {
				this.layoutUtilsService.showActionNotification("บันทึกสำเร็จ", MessageType.Update)
				this.goBack()
			})
			.catch((error) => {
				this.layoutUtilsService.showActionNotification(error.error, MessageType.Read)
			})
	}
	add(_formValue: UserExamModel, withBack: boolean = false) {
		this.userExamService.add(_formValue)
			.then((res) => {
				this.layoutUtilsService.showActionNotification("บันทึกสำเร็จ", MessageType.Create)
				this.goBack()
			})
			.catch((error) => {
				this.layoutUtilsService.showActionNotification(error.error, MessageType.Read)
			})
	}

}

export class UserExamScore{
	id?: number;
	user_id: number;
	exam_id: number;
	receipt_file: string = null;
	seat_no: string;
	score: number;
	is_reg_confirmed: number;
	is_pass: number;
	user_exam_first_name: string;
	user_exam_last_name: string;
	user_exam_card_no: string;
	card_type_id: number;

	clear() {
		this.user_id = 0;
		this.exam_id = 0;
		this.receipt_file = '';
		this.seat_no = '';
		this.score = 0;
		this.is_reg_confirmed = 0;
		this.is_pass = 0;
		this.user_exam_first_name = '' ;
		this.user_exam_last_name = '';
		this.user_exam_card_no = '';
		this.card_type_id = 0;
	}
}