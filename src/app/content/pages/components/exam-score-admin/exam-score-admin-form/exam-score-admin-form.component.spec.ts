import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamScoreAdminFormComponent } from './exam-score-admin-form.component';

describe('ExamScoreAdminFormComponent', () => {
  let component: ExamScoreAdminFormComponent;
  let fixture: ComponentFixture<ExamScoreAdminFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamScoreAdminFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamScoreAdminFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});