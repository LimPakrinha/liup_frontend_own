import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamScoreAdminComponent } from './exam-score-admin.component';

describe('ExamScoreComponent', () => {
  let component: ExamScoreAdminComponent;
  let fixture: ComponentFixture<ExamScoreAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamScoreAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamScoreAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
