import { Component, Inject, OnInit } from '@angular/core';
import { MatSnackBar, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { UtilityService } from '../../../../../service/utility.service';
import { ActionNotificationComponent } from '../../../../../content/pages/components/share/action-natification/action-notification.component';
// import { UserExamService } from '../../../../../service/user-exam/user-exam.service'
export enum MessageType {
	Create,
	Read,
	Update,
	Delete
}
@Component({
	selector: 'm-fail-payment-dialog',
	templateUrl: './fail-payment-dialog.component.html'
})

export class FailPaymentDialogComponent implements OnInit {
	viewLoading: boolean = false;
	message: string;
	reasonformGroup: FormGroup;
	constructor(
		public dialogRef: MatDialogRef<FailPaymentDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private formBuilder: FormBuilder,
		private http: HttpClient,
		@Inject('API_URL') private API_URL: string,
		private util: UtilityService,
		private snackBar: MatSnackBar
	) { 
		this.reasonformGroup = this.formBuilder.group({
			reason:['',[Validators.required]]
		});
	}

	ngOnInit() {

	}

	onNoClick(): void {
		this.dialogRef.close();
	}

	onYesClick(): void {
		const controls = this.reasonformGroup.controls;

		if (this.reasonformGroup.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}

		let data = {
			description: controls['reason'].value,
			is_reg_confirmed: 3,
			seat_no: null
		}
		this.update(this.data.user_exam_id, data).then((res) => {
			this.viewLoading = true;
			setTimeout(() => {
				this.dialogRef.close(true); // Keep only this row
				this.showActionNotification("บันทึกสำเร็จ", MessageType.Read)
			}, 2500);

		}).catch((error) => {
			this.showActionNotification(error.error, MessageType.Read)
		})
	}

	update(id: number, data): Promise<any> {
		return new Promise((resolve, reject) => {
		  this.http.put(`${this.API_URL}/user_exam/${id}`, data)
			.toPromise()
			.then((res: any) => {
			  (res.error) ? reject(res) : resolve(res);
			})
			.catch((error) => {
			  this.util.verifyExpireToken(error);
			  this.showActionNotification("ระบบเกิดข้อผิดพลาด", MessageType.Read)
			})
		});
	  }

	showActionNotification(
		message: string,
		type: MessageType = MessageType.Create,
		duration: number = 10000,
		showCloseButton: boolean = true,
		showUndoButton: boolean = false,
		undoButtonDuration: number = 3000,
		verticalPosition: 'top' | 'bottom' = 'top'
	) {
		return this.snackBar.openFromComponent(ActionNotificationComponent, {
			duration: duration,
			data: {
				message,
				snackBar: this.snackBar,
				showCloseButton: showCloseButton,
				showUndoButton: showUndoButton,
				undoButtonDuration,
				verticalPosition,
				type,
				action: 'Undo'
			},
			verticalPosition: verticalPosition
		});
	}
}
