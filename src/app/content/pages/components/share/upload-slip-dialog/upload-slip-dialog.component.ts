import { Component, Inject, ChangeDetectorRef,  OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatSnackBar, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { UtilityService } from '../../../../../service/utility.service';
import { ActionNotificationComponent } from '../../../../../content/pages/components/share/action-natification/action-notification.component';
export enum MessageType {
	Create,
	Read,
	Update,
	Delete
}
@Component({
	selector: 'm-upload-slip-dialog',
	templateUrl: './upload-slip-dialog.component.html'
})

export class UploadSlipDialogComponent implements OnInit {
	viewLoading: boolean = false;
	@ViewChild('fileInput') fileInput: ElementRef;
	// imageData: any = [];
    imageSrc: any = null;
	imageUrl: any = null;
	formGroup: FormGroup;

	constructor(
		public dialogRef: MatDialogRef<UploadSlipDialogComponent>,
		public cdr:ChangeDetectorRef,
		@Inject(MAT_DIALOG_DATA) public data: any,
		@Inject('LIMIT_FILE_SIZE') public LIMIT_FILE_SIZE: any,
		@Inject('API_URL') public API_URL: string,
		private snackBar: MatSnackBar,
		private http: HttpClient,
		private util: UtilityService
	) { 
		if(this.data.img_path)
			this.imageUrl = `${this.API_URL}/../..${this.data.img_path}`
	}

	ngOnInit() {
	}

	onNoClick(): void {
		this.dialogRef.close();
	}

	onYesClick(): void {
		/* Server loading imitation. Remove this */
		this.viewLoading = true;
		setTimeout(() => {
			
			let update_data: any;
			if (this.imageSrc) {
				update_data = {
					receipt_file: this.imageSrc,
					is_reg_confirmed: 2
				}
			}
			this.update(this.data.user_exam_id, update_data).then(res =>{
				this.showActionNotification("บันทึกสำเร็จ", MessageType.Read)
				this.dialogRef.close(true);// Keep only this row
			})
		}, 2500);
	}

	onSelectFileImage(event) {
		if (event.target.files && event.target.files[0]) {
			var reader = new FileReader();
			let fileSize = event.target.files[0].size

			if (fileSize > this.LIMIT_FILE_SIZE.LIMIT) {
				this.showActionNotification(this.LIMIT_FILE_SIZE.MESSAGE, MessageType.Read)
				// clear file select
				this.fileInput.nativeElement.value = "";
			} else {
				reader.onload = (event: ProgressEvent) => {
					this.imageSrc = (<FileReader>event.target).result;
					if (this.imageSrc) this.formGroup.controls.image.setErrors(null);
					this.cdr.detectChanges();
				}
				reader.readAsDataURL(event.target.files[0]);
			}
		}
	}

    checkSelectImage() {
		// this.formGroup.controls.image.markAsTouched()
		if (!this.imageSrc && !this.imageUrl) {
			this.formGroup.controls.image.setErrors(Validators.required);
		}
	}
	showActionNotification(
		message: string,
		type: MessageType = MessageType.Create,
		duration: number = 10000,
		showCloseButton: boolean = true,
		showUndoButton: boolean = false,
		undoButtonDuration: number = 3000,
		verticalPosition: 'top' | 'bottom' = 'top'
	) {
		return this.snackBar.openFromComponent(ActionNotificationComponent, {
			duration: duration,
			data: {
				message,
				snackBar: this.snackBar,
				showCloseButton: showCloseButton,
				showUndoButton: showUndoButton,
				undoButtonDuration,
				verticalPosition,
				type,
				action: 'Undo'
			},
			verticalPosition: verticalPosition
		});
	}
	update(id: number, data): Promise<any> {
		return new Promise((resolve, reject) => {
		  this.http.put(`${this.API_URL}/user_exam/${id}`, data)
			.toPromise()
			.then((res: any) => {
			  (res.error) ? reject(res) : resolve(res);
			})
			.catch((error) => {
			  this.util.verifyExpireToken(error);
			  this.showActionNotification("ระบบเกิดข้อผิดพลาด", MessageType.Read)
			})
		});
	  }
}
