import { ChangeDetectionStrategy, Component, OnInit, ViewChild, ElementRef, HostListener, ChangeDetectorRef, Output, Inject } from '@angular/core';
import { Router } from '@angular/router';
//nimport { examDataSource } from '../../../../core/data-sources/exam.datasource';
import { SubheaderService } from '../../../../core/services/layout/subheader.service';
import { MatTableDataSource } from '@angular/material';
import { UserExamService } from '../../../../service/user-exam/user-exam.service';
// import { LayoutConfigService } from '../../../../core/services/layout-config.service';
import * as objectPath from 'object-path';
import { NONE_TYPE } from '@angular/compiler/src/output/output_ast';
//import { DataSource } from '@angular/cdk/table';
import { DataSource } from '@angular/cdk/collections';
import { fromEvent, merge } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { UserExamModel } from '../../../../core/models/user-exam.model';
import { SelectionModel } from '@angular/cdk/collections';
import { ExamPlaceDataSource } from '../../../../core/data-sources/exam-place.datasource';
import { QueryParamsModel } from '../../../../core/models/query-params.model';
import { ActivatedRoute } from '@angular/router';
import { MatPaginator, MatSort, MatSnackBar, MatDialog } from '@angular/material';



@Component({
	selector: 'm-exam-place',
	templateUrl: './exam-place.component.html',
	styleUrls: ['./exam-place.component.scss']
})
export class ExamPlaceComponent implements OnInit {

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
	// Filter fields
	@ViewChild('searchInput') searchInput: ElementRef;
	filterStatus: string = '';
	filterCondition: string = '';
	// Selection
	selection = new SelectionModel<UserExamModel>(true, []);
	dataResult: UserExamModel[] = [];

	dataSource: ExamPlaceDataSource;

	public config: any;
	currentUserData: any = null;
	displayedColumns = ['name', 'exam_place','exam_time','exam_date', 'seat_no'];

	constructor(
		private router: Router,
		private subheaderService: SubheaderService,
		private cdr: ChangeDetectorRef,
		private SubheaderService: SubheaderService,
		private userExamService: UserExamService,
		private route: ActivatedRoute,
		@Inject('API_URL') public API_URL: string,
	) {
		this.currentUserData = JSON.parse(localStorage.getItem('userlogin'))
	}


	ngOnInit() {
		// this.list()
		this.initSubHeader()
		this.getComponentTitle()

		// If the user changes the sort order, reset back to the first page.
		this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

		/* Data load will be triggered in two cases:
		- when a pagination event occurs => this.paginator.page
		- when a sort event occurs => this.sort.sortChange
		**/
		merge(this.sort.sortChange, this.paginator.page)
			.pipe(
				tap(() => {
					this.loadExamPlace();
				})
			)
			.subscribe();

		// Filtration, bind to searchInput
		fromEvent(this.searchInput.nativeElement, 'keyup')
			.pipe(
				debounceTime(150),
				distinctUntilChanged(),
				tap(() => {
					this.paginator.pageIndex = 0;
					this.loadExamPlace();
				})
			)
			.subscribe();

		// Set title to page breadCrumbs
		// Init DataSource
		this.dataSource = new ExamPlaceDataSource(this.userExamService);
		console.log(this.dataSource)
		let queryParams = new QueryParamsModel({});
		// Read from URL itemId, for restore previous state
		this.route.queryParams.subscribe(params => {
			if (params.id) {
				// queryParams = this.userService.lastFilter$.getValue();
				// this.restoreState(queryParams, +params.id);
			}
			// First load
			this.dataSource.loadData(queryParams);
			console.log(this.dataSource.loadData(queryParams))
		});
		this.dataSource.entitySubject.subscribe(res => this.dataResult = res);
	}

	/** FILTRATION */
	filterConfiguration(): any {
		const filter: any = {};
		const searchText: string = this.searchInput.nativeElement.value;
		// filter['exam.exam_place'] = searchText;
		filter['exam.name'] = searchText;
		return filter;
	}

	/** SELECTION */
	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataResult.length;
		return numSelected === numRows;
	}

	/** Selects all rows if they are not all selected; otherwise clear selection. */
	masterToggle() {
		if (this.isAllSelected()) {
			this.selection.clear();
		} else {
			this.dataResult.forEach(row => this.selection.select(row));
		}
	}
	loadExamPlace() {
		this.selection.clear();
		const queryParams = new QueryParamsModel(
			this.filterConfiguration(),
			this.sort.direction,
			this.sort.active,
			this.paginator.pageIndex,
			this.paginator.pageSize
		);
		this.dataSource.loadData(queryParams);
	}

	// list() {
	// 	this.userExamService.getUserExam().then((res: any) => {
	// 		this.dataSource.data = res.data;
	// 		console.log(this.dataSource.data)
	// 		console.log("No data")
	// 		if (this.dataSource.data.length == 0) {
	// 			this.dataSource.data.push({
	// 				'name': '-',
	// 				'exam_place': '-',
	// 				'exam_time': '-',
	// 				'exam_date': '-',
	// 				'seat_no': '-',
	// 			})
	// 		}
	// 		this.initFormList();
	// 	})
	// 		.catch((error) => {
	// 			console.log(error);
	// 		})
	// }

	updatePage(data) {
		this.router.navigate(['/exam-place', data]);
	}
	initSubHeader() {
		this.subheaderService.setTitle('สถานที่และเลขที่นั่งสอบ');
		this.subheaderService.setBreadcrumbs([
			{ title: 'สถานที่และเลขที่นั่งสอบ' }
		]);
	}

	getComponentTitle() {
		let result = `สถานที่และเลขที่นั่งสอบ`;
		return result;
	}
	// initFormList() {
	// 	this.dataSource.paginator = this.paginator;
	// 	this.dataSource.sort = this.sort;

	// }
}
