import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamPlaceComponent } from './exam-place.component';

describe('ExamPlaceComponent', () => {
  let component: ExamPlaceComponent;
  let fixture: ComponentFixture<ExamPlaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamPlaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamPlaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
