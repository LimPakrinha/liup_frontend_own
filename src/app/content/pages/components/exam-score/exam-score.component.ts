import { ChangeDetectionStrategy, Component, OnInit, ViewChild, ElementRef, HostListener, ChangeDetectorRef, Output, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { SubheaderService } from '../../../../core/services/layout/subheader.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { UserExamService } from '../../../../service/user-exam/user-exam.service';
import * as objectPath from 'object-path';
import { NONE_TYPE } from '@angular/compiler/src/output/output_ast';
import { DataSource } from '@angular/cdk/collections';
import { fromEvent, merge } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { UserExamModel } from '../../../../core/models/user-exam.model';
import { SelectionModel } from '@angular/cdk/collections';
import { ExamScoreDataSource } from '../../../../core/data-sources/exam-score.datasource';
import { QueryParamsModel } from '../../../../core/models/query-params.model';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar, MatDialog } from '@angular/material';



@Component({
	selector: 'm-exam-score',
	templateUrl: './exam-score.component.html',
	styleUrls: ['./exam-score.component.scss']
})
export class ExamScoreComponent implements OnInit {

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;

	// Filter fields
	@ViewChild('searchInput') searchInput: ElementRef;
	filterStatus: string = '';
	filterCondition: string = '';
	// Selection
	selection = new SelectionModel<UserExamModel>(true, []);
	dataResult: UserExamModel[] = [];

	dataSource: ExamScoreDataSource;

	public config: any;
	currentUserData: any = null;
	displayedColumns = ['name', 'exam_date','exam_time', 'announce_score_date', 'score','is_pass'];

	constructor(
		private router: Router,
		private subheaderService: SubheaderService,
		private cdr: ChangeDetectorRef,
		private SubheaderService: SubheaderService,
		private userExamService: UserExamService,
		private route: ActivatedRoute,

		@Inject('API_URL') public API_URL: string,
	) {
		this.currentUserData = JSON.parse(localStorage.getItem('userlogin'))
	}


	ngOnInit() {
		// this.list()
		this.initSubHeader()
		this.getComponentTitle()

		// If the user changes the sort order, reset back to the first page.
		this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

		/* Data load will be triggered in two cases:
		- when a pagination event occurs => this.paginator.page
		- when a sort event occurs => this.sort.sortChange
		**/
		merge(this.sort.sortChange, this.paginator.page)
			.pipe(
				tap(() => {
					this.loadExamScore();
				})
			)
			.subscribe();

		// Filtration, bind to searchInput
		fromEvent(this.searchInput.nativeElement, 'keyup')
			.pipe(
				debounceTime(150),
				distinctUntilChanged(),
				tap(() => {
					this.paginator.pageIndex = 0;
					this.loadExamScore();
				})
			)
			.subscribe();

		// Set title to page breadCrumbs
		// Init DataSource
		this.dataSource = new ExamScoreDataSource(this.userExamService);
		console.log(this.dataSource)
		let queryParams = new QueryParamsModel({});
		// Read from URL itemId, for restore previous state
		this.route.queryParams.subscribe(params => {
			if (params.id) {
				// queryParams = this.userService.lastFilter$.getValue();
				// this.restoreState(queryParams, +params.id);
			}
			// First load
			this.dataSource.loadData(queryParams);
			console.log(this.dataSource.loadData(queryParams))
		});
		this.dataSource.entitySubject.subscribe(res => this.dataResult = res);
	}

	/** FILTRATION */
	filterConfiguration(): any {
		const filter: any = {};
		const searchText: string = this.searchInput.nativeElement.value;
		filter['exam.name'] = searchText;
		return filter;
	}

	/** SELECTION */
	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataResult.length;
		return numSelected === numRows;
	}

	/** Selects all rows if they are not all selected; otherwise clear selection. */
	masterToggle() {
		if (this.isAllSelected()) {
			this.selection.clear();
		} else {
			this.dataResult.forEach(row => this.selection.select(row));
		}
	}
	loadExamScore() {
		this.selection.clear();
		const queryParams = new QueryParamsModel(
			this.filterConfiguration(),
			this.sort.direction,
			this.sort.active,
			this.paginator.pageIndex,
			this.paginator.pageSize
		);
		this.dataSource.loadData(queryParams);
	}

	// list() {
	// 	this.userExamService.getUserExam().then((res: any) => {
	// 		this.dataSource.data = res.data;
	// 		for (let i = 0; i < this.dataSource.data.length; i++) {
	// 			this.dataSource.data[i].exam_time = res.data[i].exam_start_time.substr(0,5) + " - " + res.data[i].exam_end_time.substr(0,5);
	// 			if (this.dataSource.data[i].is_pass === 1) {
	// 				this.dataSource.data[i].is_pass = "ผ่าน";
	// 			} else if (this.dataSource.data[i].is_pass === 0) {
				
	// 				this.dataSource.data[i].is_pass = "ไม่ผ่าน";
	// 			}
	// 			if (this.dataSource.data[i].score == 0 || this.dataSource.data[i].score == null) {
	// 				this.dataSource.data[i].score = '-';
	// 				this.dataSource.data[i].is_pass = '-';
	// 			}
	// 			// this.dataSource.data[i].announce_score_date = this.dataSource.data[i].announce_score_date.substring(0, 10);
	// 		}
	// 		if (this.dataSource.data.length == 0) {
	// 			this.dataSource.data.push({
	// 				'name': '-',
	// 				'exam_date': '-',
	// 				'announce_score_date': '-',
	// 				'score': '-',
	// 				'is_pass': '-'
	// 			})

	// 		}
	// 		this.initFormList();
	// 	})
	// 		.catch((error) => {
	// 			console.log(error);
	// 		})
	// }

	updatePage(data) {
		this.router.navigate(['/exam-score', data]);
	}
	initSubHeader() {
		this.subheaderService.setTitle('คะแนนสอบ');
		this.subheaderService.setBreadcrumbs([
			{ title: 'คะแนนสอบ' }
		]);
	}

	getComponentTitle() {
		let result = `คะแนนสอบ`;
		return result;
	}
}
