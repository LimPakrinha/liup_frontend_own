import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { ActivatedRoute } from '@angular/router';
import { LayoutUtilsService, MessageType } from '../../../../../core/services/layout/layout-utils.service';
import { fromEvent, merge } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { QueryParamsModel } from '../../../../../core/models/query-params.model';
import { UserExamDataSource } from '../../../../../core/data-sources/user-exam.datasource';
import { UserExamModel } from '../../../../../core/models/user-exam.model';
import { UserExamService } from '../../../../../service/user-exam/user-exam.service';
import { SubheaderService } from '../../../../../core/services/layout/subheader.service';

@Component({
	selector: 'm-user-exam-register-list',
	templateUrl: './user-exam-register-list.component.html',
	styleUrls: ['./user-exam-register-list.component.scss']
})
export class UserExamRegisterListComponent implements OnInit {

	dataSource: UserExamDataSource;
	displayedColumns = ['name', 'exam_place','register_open_date', 'register_close_date', 'exam_date', 'exam_time', 'cost', 'status_seat', 'seat', 'actions', 'file','description'];
	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
	// Filter fields
	@ViewChild('searchInput') searchInput: ElementRef;
	filterStatus: string = '';
	filterCondition: string = '';
	// Selection
	selection = new SelectionModel<UserExamModel>(true, []);
	dataResult: UserExamModel[] = [];

	constructor(
		@Inject('FILEUPLOAD_URL') public FILEUPLOAD_URL: string,
		public dialog: MatDialog,
		private route: ActivatedRoute,
		private subheaderService: SubheaderService,
		private layoutUtilsService: LayoutUtilsService,
		private userExamService: UserExamService,
		
	) { }

	ngOnInit() {
		// If the user changes the sort order, reset back to the first page.
		this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

		/* Data load will be triggered in two cases:
		- when a pagination event occurs => this.paginator.page
		- when a sort event occurs => this.sort.sortChange
		**/
		merge(this.sort.sortChange, this.paginator.page)
			.pipe(
				tap(() => {
					this.loadExamList();
				})
			)
			.subscribe();

		// Filtration, bind to searchInput
		fromEvent(this.searchInput.nativeElement, 'keyup')
			.pipe(
				debounceTime(150),
				distinctUntilChanged(),
				tap(() => {
					this.paginator.pageIndex = 0;
					this.loadExamList();
				})
			)
			.subscribe();

		// Set title to page breadCrumbs
		// Init DataSource
		this.dataSource = new UserExamDataSource(this.userExamService);
		let queryParams = new QueryParamsModel({});
		// Read from URL itemId, for restore previous state
		this.route.queryParams.subscribe(params => {
			if (params.id) {
				// queryParams = this.userService.lastFilter$.getValue();
				// this.restoreState(queryParams, e+params.id);
			}
			// First load
			this.dataSource.loadData(queryParams);
			
		});
		this.dataSource.entitySubject.subscribe(res => this.dataResult = res);
		// console.log(this.dataSource.entitySubject._value[0].exam_+place,"datasource");
		this.initSubHeader();
		
	}

	/** FILTRATION */
	filterConfiguration(): any {
		const filter: any = {};
		const searchText: string = this.searchInput.nativeElement.value;
		filter['exam.name'] = searchText;
		filter['exam.exam_place'] = searchText;
		return filter;
	}

	/** SELECTION */
	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataResult.length;
		return numSelected === numRows;
	}

	/** Selects all rows if they are not all selected; otherwise clear selection. */
	masterToggle() {
		if (this.isAllSelected()) {
			this.selection.clear();
		} else {
			this.dataResult.forEach(row => this.selection.select(row));
		}
	}

	loadExamList() {
		this.selection.clear();
		const queryParams = new QueryParamsModel(
			this.filterConfiguration(),
			this.sort.direction,
			this.sort.active,
			this.paginator.pageIndex,
			this.paginator.pageSize
		);
		this.dataSource.loadData(queryParams);
	}

	registerExam(examId: number) {
		this.userExamService.getExamInfo(examId).subscribe((res) => {
			let exam = res.data;
			const _title: string = 'ยืนยันการสมัครสอบ';
			const _description: string = `<p>คุณต้องการสมัครสอบรายการ ${exam.name}<p><p>สถานที่สอบ ${exam.exam_place}</p><p>วันที่สอบ ${exam.exam_date}</p><p>ค่าสมัครสอบ ${exam.cost} บาท</p><p>** กรุณาชำระเงินที่ศูนย์ศิลปศาสตร์</p>`;
			const _waitDesciption: string = 'กำลังบันทึกข้อมูล...';

			const dialogRef = this.layoutUtilsService.modalElement(_title, _description, _waitDesciption);
			dialogRef.afterClosed().subscribe(res => {
				if (!res) {
					return;
				}
				// coding add register exam data
				this.userExamService.add({ exam_id: examId, is_reg_confirmed: 1 }).then((res) => {
					if (res.status === "success") {
						this.layoutUtilsService.showActionNotification("ทำการสมัครสอบเรียบร้อยแล้ว", MessageType.Create)
						this.loadExamList();
					}
				}).catch((error) => {
					this.layoutUtilsService.showActionNotification("เกิดข้อผิดพลาด", MessageType.Read)
					this.loadExamList();
				})
			});
		});
	}
	initSubHeader() {
		this.subheaderService.setTitle('สมัครสอบ');
		this.subheaderService.setBreadcrumbs([
			{ title: 'สมัครสอบ' }
		]);
		this.getComponentTitle();
	}

	getComponentTitle() {
		let result = `สมัครสอบ`;
		return result;
	}
	
	uploadSlip(user_exam_id, receipt_file?){
		const _title: string = 'ยืนยันการสมัครสอบ';
		const _description: string = `สลิปหลักฐาน`;
		const _waitDesciption: string = 'กำลังบันทึกข้อมูล...';

		const dialogRef = this.layoutUtilsService.uploadSlip(_title, _description, _waitDesciption, user_exam_id, receipt_file);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}else{
				this.loadExamList();
			}
		})
	}

	showdescription(text){
		const _title: string = 'หมายเหตุ';
		const _description: string = `<p>${text}</p>`;
		const _waitDesciption: string = '';

		const dialogRef = this.layoutUtilsService.modalElement(_title,  _description, _waitDesciption);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}else{
				this.loadExamList();
			}
		})
	}
}
