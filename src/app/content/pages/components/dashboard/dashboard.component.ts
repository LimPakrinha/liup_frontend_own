import { ChangeDetectionStrategy, Component, OnInit,  ViewChild, ElementRef, HostListener, ChangeDetectorRef, Output } from '@angular/core';
import { Router } from '@angular/router';
import { SubheaderService } from '../../../../core/services/layout/subheader.service';
import { MatPaginator, MatSort } from '@angular/material';
import { examService } from '../../../../service/exam/exam.service';
import { LayoutUtilsService, MessageType } from '../../../../core/services/layout/layout-utils.service';
import { ExamModel } from '../../../../core/models/exam.model';
import { SelectionModel } from '@angular/cdk/collections';
import { fromEvent, merge } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { DashboardDataSource } from '../../../../core/data-sources/dashboard.datasource';
import { QueryParamsModel } from '../../../../core/models/query-params.model';
import { ActivatedRoute } from '@angular/router';
@Component({
	selector: 'm-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})

export class DashboardComponent implements OnInit {
	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;

	dataSource: DashboardDataSource;
	// Filter fields
	@ViewChild('searchInput') searchInput: ElementRef;
	filterStatus: string = '';
	filterCondition: string = '';
	// Selection
	selection = new SelectionModel<ExamModel>(true, []);
	dataResult: ExamModel[] = [];

	public config: any;

	isActive: any[]=[{id:1,name:'เปิด'},{id:2,name:'ปิด'}];
	displayedColumns= ['name','exam_type_name', 'exam_place','register_open_date','register_close_date','exam_date','exam_time','announce_score_date','cost','seat','is_active','status_seat_name','action'];

	constructor(
		private router: Router,
		private subheaderService:SubheaderService,
		private examService: examService,
		private layoutUtilsService: LayoutUtilsService,
		private route: ActivatedRoute
	) {
	}
	ngOnInit(){
		// this.list()
		this.initSubHeader()
		this.getComponentTitle()
		// If the user changes the sort order, reset back to the first page.
		this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

		/* Data load will be triggered in two cases:
		- when a pagination event occurs => this.paginator.page
		- when a sort event occurs => this.sort.sortChange
		**/
		merge(this.sort.sortChange, this.paginator.page)
			.pipe(
				tap(() => {
					this.List();
				})
			)
			.subscribe();

		// Filtration, bind to searchInput
		fromEvent(this.searchInput.nativeElement, 'keyup')
			.pipe(
				debounceTime(150),
				distinctUntilChanged(),
				tap(() => {
					this.paginator.pageIndex = 0;
					this.List();
				})
			)
			.subscribe();

		// Set title to page breadCrumbs
		// Init DataSource
		this.dataSource = new DashboardDataSource(this.examService);
		console.log(this.dataSource)
		let queryParams = new QueryParamsModel({});
		// Read from URL itemId, for restore previous state
		this.route.queryParams.subscribe(params => {
			if (params.id) {
				// queryParams = this.userService.lastFilter$.getValue();
				// this.restoreState(queryParams, +params.id);
			}
			// First load
			this.dataSource.loadData(queryParams);
		});
		this.dataSource.entitySubject.subscribe(res => this.dataResult = res);
	}

	// list()
	// {	
	// 	this.examService.list().then((res) =>{
	// 		this.dataSource.data =res.data;
	// 		for (let i= 0; i < this.dataSource.data.length ; i++){
	// 			this.dataSource.data[i].exam_time = res.data[i].exam_start_time.substr(0,5) + " - " + res.data[i].exam_end_time.substr(0,5);
	// 		}

	// 		console.log(this.dataSource)
	// 		this.initFormList();
	// 	})
	// 	.catch((error)=>{
	// 		console.log(error);
	// 		})
	// }

	updatePage(data){

		this.router.navigate(['/exam-form', data]);
	}
	initSubHeader() {
		this.subheaderService.setTitle('รายการสมัครสอบ');
		this.subheaderService.setBreadcrumbs([
		  { title: 'รายการสมัครสอบ' ,page:'/exam-data'}
		]);
	  }
	
	  getComponentTitle() {
		let result = `รายการสมัครสอบ`;
		return result;
	  }
	  delete(data) {
		const _title: string = 'การยืนยันลบรายการ';
		const _description: string = 'ต้องการลบรายการนี้ ใช่หรือไม่ ?';
		const _waitDesciption: string = 'กำลังดำเนินการลบ...';
	
		const dialogRef = this.layoutUtilsService.deleteElement(_title, _description, _waitDesciption);
		dialogRef.afterClosed().subscribe(res => {
		  if (!res) {
			return;
		  }
		  else{
			this.examService.delete(data)
			  .then((response) => {
				if (response.status === "success") {
				  this.layoutUtilsService.showActionNotification("รายการสมัครสอบถูกลบออกจากระบบ", MessageType.Read)
				  this.List()
				}
			  })
			  .catch(error => {
				if(error.error.title){
				  this.layoutUtilsService.showActionNotification(error.error.title, MessageType.Read)
				}else{
				  this.layoutUtilsService.showActionNotification("System Error", MessageType.Read)
				}
				this.List()
			  });
		  }
		})
	  }
	/** FILTRATION */
	filterConfiguration(): any {
		const filter: any = {};
		const searchText: string = this.searchInput.nativeElement.value;
		filter['exam.name'] = searchText;
		filter['exam.exam_place'] = searchText;
		return filter;
	}

	/** SELECTION */
	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataResult.length;
		return numSelected === numRows;
	}

	/** Selects all rows if they are not all selected; otherwise clear selection. */
	masterToggle() {
		if (this.isAllSelected()) {
			this.selection.clear();
		} else {
			this.dataResult.forEach(row => this.selection.select(row));
		}
	}
	List() {
		this.selection.clear();
		const queryParams = new QueryParamsModel(
			this.filterConfiguration(),
			this.sort.direction,
			this.sort.active,
			this.paginator.pageIndex,
			this.paginator.pageSize
		);
		this.dataSource.loadData(queryParams);
	}
	
}

