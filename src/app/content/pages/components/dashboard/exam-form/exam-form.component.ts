import { Component, OnInit, ChangeDetectorRef, Inject} from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SubheaderService } from '../../../../../core/services/layout/subheader.service';
import { LayoutUtilsService, MessageType } from '../../../../../core/services/layout/layout-utils.service';
import { BehaviorSubject } from 'rxjs';
import { ExamModel } from '../../../../../core/models/exam.model';
import { examService } from '../../../../../service/exam/exam.service';
import { status_seatService } from '../../../../../service/status_seat/status_seat.service';
import { examTypeService } from '../../../../../service/exam-type/exam-type.service';
import * as moment from 'moment';

@Component({
	selector: 'm-exam-form',
	templateUrl: './exam-form.component.html',
	styleUrls: ['./exam-form.component.scss']
})
export class ExamFormComponent implements OnInit {
	examformModel: ExamModel;
	examformModelOld: ExamModel;
	loadingSubject = new BehaviorSubject<boolean>(false);
	loading$ = this.loadingSubject.asObservable();
	ExamformGroup: FormGroup;
	StatusSeatData: any = [];
	ExamTypeData: any = [];
	format = 'YYYY/MM/DD';
	private examID:number;
	constructor(
		@Inject('API_URL') private API_URL: string,
		private router: Router,
		private activatedRoute: ActivatedRoute,
		private subheaderService: SubheaderService,
		private formBuilder: FormBuilder,
		private changeDetectorRef: ChangeDetectorRef,
		private examService: examService,
		private examTypeService: examTypeService,
		private StatusSeatService: status_seatService,
		private layoutUtilsService: LayoutUtilsService,
	) { 

	}

	ngOnInit() {
		this.createForm();
		this.loadingSubject.next(true);
		this.getSeat_status();
		this.initSubHeader()
	}

	formInit(){
		this.activatedRoute.queryParams.subscribe(params => {
			this.examID = + params.id;
			if (this.examID && this.examID > 0) {
				this.examService.get(this.examID).then(res => {
					this.examformModel = res.data;
					this.initPage();

				});
			}else {
				const newModel = new ExamModel();
				newModel.clear();
				this.examformModel = newModel;

			}
		});

	}

	getSeat_status() {
		this.StatusSeatService.list()
			.then((res) => {
				if (res.status == 'success') {
					this.StatusSeatData = res.data
					this.getExamType();
					this.formInit();
				} else {
					this.layoutUtilsService.showActionNotification(res.error, MessageType.Read)
				}
				this.changeDetectorRef.detectChanges();
			})
	}
	getExamType() {
		this.examTypeService.list()
			.then((res) => {
				if (res.status == 'success') {
					this.ExamTypeData = res.data
				} else {
					this.layoutUtilsService.showActionNotification(res.error, MessageType.Read)
				}
				this.changeDetectorRef.detectChanges();
			})
	}
	goBack(id = 0) {
		let _backUrl = '';
		if (id > 0) {
			_backUrl += '?id=' + id;
		}
		this.router.navigateByUrl(_backUrl);
	}

	initPage() {
		this.createForm();
		this.loadingSubject.next(false);
	}

	initSubHeader() {
		this.subheaderService.setTitle('รายการสมัครสอบ');
		this.subheaderService.setBreadcrumbs([
			{ title: 'รายการสมัครสอบ', page: "/exam-data" },
			{ title: 'การจัดการรายการสอบ' }
		]);
		this.getComponentTitle();
	}

	getComponentTitle() {
		let result = `การจัดการรายการสอบ`;
		return result;
	}

	createForm() {
		if (this.examformModel) {
			this.ExamformGroup = this.formBuilder.group({
				name: [this.examformModel.name, [Validators.required, Validators.maxLength(255)]],
				exam_type_name: [this.examformModel.exam_type_id, [Validators.required]],
				exam_place: [this.examformModel.exam_place, [Validators.required, Validators.maxLength(255)]],
				exam_start_time: [this.examformModel.exam_start_time, [Validators.required, Validators.maxLength(255)]],
				exam_end_time: [this.examformModel.exam_end_time, [Validators.required, Validators.maxLength(255)]],
				register_open_date: [moment(this.examformModel.register_open_date).toISOString(), [Validators.required]],
				register_close_date: [moment(this.examformModel.register_close_date).toISOString(), [Validators.required]],
				exam_date: [moment(this.examformModel.exam_date).toISOString(), [Validators.required]],
				announce_score_date: [moment(this.examformModel.announce_score_date).toISOString(), [Validators.required]],
				cost: [this.examformModel.cost, [Validators.required, Validators.pattern('[0-9]*')]],
				seat: [this.examformModel.seat, [Validators.required, Validators.pattern('[0-9]*')]],
				is_active: [this.examformModel.is_active, [Validators.required]],
				status_seat_name: [this.examformModel.status_seat_id, { value: this.examformModel.status_seat_id }]
			});

			this.changeDetectorRef.detectChanges();

		} else {
			this.ExamformGroup = this.formBuilder.group({
				name: ["", [Validators.required, Validators.maxLength(255)]],
				exam_type_name: ["", [Validators.required]],
				exam_place: ["", [Validators.required, Validators.maxLength(255)]],
				exam_start_time: ["", [Validators.required, Validators.maxLength(255)]],
				exam_end_time: ["", [Validators.required, Validators.maxLength(255)]],
				register_open_date: ["", [Validators.required]],
				register_close_date: ["", [Validators.required]],
				exam_date: ["", [Validators.required]],
				announce_score_date: ["", [Validators.required]],
				cost: ["", [Validators.required, Validators.pattern('[0-9]*')]],
				seat: ["", [Validators.required, Validators.pattern('[0-9]*')]],
				is_active: ["", [Validators.required]],
				status_seat_name: ["", [Validators.required]]
			});

		}

	}
	onSumbit(withBack: boolean = false) {

		const controls = this.ExamformGroup.controls;
		// console.log(controls, "control")
		// tslint:disable-next-line:prefer-const
		let editedData = this.prepareUser();
		/** check form */
		// console.log(this.ExamformGroup.invalid, "form invalid")
		if (this.ExamformGroup.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}

		if (editedData.id > 0) {
			this.update(editedData, withBack);

			return;
		}
		this.add(editedData, withBack);
	}
	prepareUser(): ExamModel {
		const controls = this.ExamformGroup.controls;
		const _formValue = new ExamModel();

		_formValue.id = this.examformModel.id;
		_formValue.name = controls['name'].value;
		_formValue.exam_type_id = controls['exam_type_name'].value;
		_formValue.exam_place = controls['exam_place'].value;
		_formValue.exam_start_time = controls['exam_start_time'].value;
		_formValue.exam_end_time = controls['exam_end_time'].value;
		_formValue.register_open_date = moment(controls['register_open_date'].value).format(this.format);
		_formValue.register_close_date = moment(controls['register_close_date'].value).format(this.format);
		_formValue.exam_date = moment(controls['exam_date'].value).format(this.format);
		_formValue.announce_score_date = moment(controls['announce_score_date'].value).format(this.format);
		_formValue.cost = controls['cost'].value;
		_formValue.seat = controls['seat'].value;
		_formValue.is_active = controls['is_active'].value;
		_formValue.status_seat_id = controls['status_seat_name'].value;
		return _formValue;
	}

	update(_formValue: ExamModel, withBack: boolean = false) {
		this.examService.update(_formValue.id, _formValue)
			.then((res) => {
				this.layoutUtilsService.showActionNotification("บันทึกสำเร็จ", MessageType.Update)
				this.goBack()
			})
			.catch((error) => {
				this.layoutUtilsService.showActionNotification(error.error, MessageType.Read)
			})
	}
	add(_formValue: ExamModel, withBack: boolean = false) {
		// console.log(_formValue)
		this.examService.add(_formValue)
			.then((res) => {
				this.layoutUtilsService.showActionNotification("บันทึกสำเร็จ", MessageType.Create)
				this.goBack()
			})
			.catch((error) => {
				this.layoutUtilsService.showActionNotification(error.error, MessageType.Read)
			})
	}

}
