import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { RouterModule,Routes } from '@angular/router';
import {FormsModule,ReactiveFormsModule }from '@angular/forms';
import { LayoutModule } from '../../../layout/layout.module';
import { PartialsModule } from '../../../partials/partials.module';
import { ListTimelineModule } from '../../../partials/layout/quick-sidebar/list-timeline/list-timeline.module';
import { WidgetChartsModule } from '../../../partials/content/widgets/charts/widget-charts.module';
import { GoogleChartsModule } from 'angular-google-charts';
import {
	MatInputModule,
	MatPaginatorModule,
	MatProgressSpinnerModule,
	MatSortModule,
	MatTableModule,
	MatSelectModule,
	MatMenuModule,
	MatProgressBarModule,
	MatButtonModule,
	MatCheckboxModule,
	MatDialogModule,
	MatTabsModule,
	MatNativeDateModule,
	MatCardModule,
	MatRadioModule,
	MatIconModule,
	MatDatepickerModule,
	MatAutocompleteModule,
	MatSlideToggleModule,
	MAT_DIALOG_DEFAULT_OPTIONS,
	MatSnackBarModule,
	MatTooltipModule
} from '@angular/material';
import {ExamFormComponent} from './exam-form/exam-form.component';
import {UserExamComponent} from './user-exam/user-exam.component';

@NgModule({
	imports: [
		CommonModule,
		MatProgressSpinnerModule,
		LayoutModule,
		PartialsModule,
		MatPaginatorModule,
		MatSortModule,
		MatButtonModule,
		ListTimelineModule,
		MatTableModule,
		WidgetChartsModule,
		MatInputModule,
		GoogleChartsModule,
		MatIconModule,
		MatSelectModule,
		MatMenuModule,
		MatProgressBarModule,
		MatDatepickerModule,
		ReactiveFormsModule,
		FormsModule,
		MatTooltipModule,
		MatCheckboxModule,
		MatDialogModule,
		MatTabsModule,
		MatNativeDateModule,
		MatCardModule,
		MatRadioModule,
		MatAutocompleteModule,
		MatSlideToggleModule,

		MatSnackBarModule,
		RouterModule.forChild([
			{
				path: '',
				component: DashboardComponent
			}
		])
	],
	providers: [],
	declarations: [DashboardComponent]
})
export class DashboardModule {}
