import { ChangeDetectionStrategy, Component, OnInit, ViewChild, ElementRef, HostListener, ChangeDetectorRef, Output } from '@angular/core';
import { SubheaderService } from '../../../../../core/services/layout/subheader.service';
import { MatPaginator, MatSort, MatTableDataSource, MatSlideToggleChange } from '@angular/material';
import { UserExamService } from '../../../../../service/user-exam/user-exam.service';
import { examService } from '../../../../../service/exam/exam.service';
import { RegConfirmedService } from '../../../../../service/reg_confirmed/reg_confirmed.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { LayoutUtilsService, MessageType } from '../../../../../core/services/layout/layout-utils.service';
import { Injectable, Inject } from '@angular/core';
import { UserExamModel } from '../../../../../core/models/user-exam.model';
@Component({
	selector: 'm-user-exam',
	templateUrl: './user-exam.component.html',
	styleUrls: ['./user-exam.component.scss']
	// changeDetection: ChangeDetectionStrategy.OnPush
})

export class UserExamComponent implements OnInit {
	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
	loadingSubject = new BehaviorSubject<boolean>(false);
	loading$ = this.loadingSubject.asObservable();
	dataSource = new MatTableDataSource<any>([]);
	reg_confirmed: any = {};;
	ExamData: any = {};
	public model: any = {};
	examID: number;
	remain_seat: number;
	taken_seat: number;
	private confirm = 4;
	private pending = 0;

	displayedColumns = ['fname', 'lname', 'seat_no', 'receipt_file', 'action'];
	constructor(
		@Inject('FILEUPLOAD_URL') public FILEUPLOAD_URL: string,
		private router: Router,
		private activatedRoute: ActivatedRoute,
		private cdr: ChangeDetectorRef,
		private SubheaderService: SubheaderService,
		private userExamService: UserExamService,
		private examService: examService,
		private regConfirmedService: RegConfirmedService,
		private layoutUtilsService: LayoutUtilsService
	) {
		this.activatedRoute.queryParams.subscribe(params => {
			this.examID = + params.id;
		});
	}

	ngOnInit() {
		this.loadingSubject.next(true);
		if (this.examID && this.examID > 0) {
			this.examService.get(this.examID).then((res) => {
				this.ExamData = res.data;
				//this.examtitle=this.ExamData.name;
				// this.regConfirmedService.getAll().then((res)=>{
				// 	this.reg_confirmed = res.data;
				// 	this.list(this.examID)
				// });
				this.list(this.examID)
			});
			this.loadingSubject.next(false);
			this.initSubHeader()

		} else {
			this.goBack()
		}

	}

	goBack() {

		let _backUrl = '';
		this.router.navigateByUrl(_backUrl);
	}

	list(id?) {
		// let remain_seat;
		this.taken_seat = 0;
		this.userExamService.getExamUser(id).then((res) => {
			this.dataSource.data = res.data;
			this.remain_seat = null;
			for (let i = 0; i < this.dataSource.data.length; i++) {
				this.dataSource.data[i].index = i;
				if (this.dataSource.data[i].is_reg_confirmed === this.confirm) {
					this.dataSource.data[i].is_reg_confirmed_boolean = true
					this.taken_seat += 1;
				}
				else {
					this.dataSource.data[i].is_reg_confirmed_boolean = false
				}
			}

			this.remain_seat = this.ExamData.seat - this.taken_seat;
			if (this.remain_seat < 0) {
				this.remain_seat = 0;
			}
			this.cdr.detectChanges();
			this.initFormList();
		})
			.catch((error) => {
				this.layoutUtilsService.showActionNotification("ระบบเกิดข้อผิดพลาด", MessageType.Read);
			});
		console.log(this.dataSource)
	}

	// confirm_check(id, i) {
	// 	if (this.dataSource.data[i].is_reg_confirmed === this.confirm) {
	// 		//check when status = full then check out the comfirmed person back
	// 		this.dataSource.data[i].seat_no = null;
	// 		this.update(id, i, 'withdraw')

	// 		this.cdr.detectChanges();
	// 		return;
	// 	}

	// 	if (this.dataSource.data[i].is_reg_confirmed != this.confirm) {
	// 		for (let j = 1; j <= this.dataSource.data.length; j++) {
	// 			let k = j;
	// 			if (this.dataSource.data.find(fruit => fruit.seat_no === k.toString()) == null) {
	// 				console.log("work");
	// 				this.dataSource.data[i].seat_no = k.toString();
	// 				break;
	// 			}
	// 		}
	// 	}

	// 	if ((this.ExamData.seat > 0 && this.remain_seat === 0) || this.ExamData.status_seat_id === 2) {
	// 		// full can not comfirmed more candidate

	// 		if (this.ExamData.status_seat_id === 1)
	// 			this.updateStatusSeat(0);
	// 		this.layoutUtilsService.showActionNotification("ที่นังเต็มแล้ว", MessageType.Read);
	// 		this.list(this.examID);
	// 		return;
	// 	}

	// 	if (this.ExamData.status_seat_id === 1 && this.remain_seat > 0) {
	// 		// limited candidate

	// 		this.update(id, i, 'limit');
	// 		//let remain_seat=this.list(this.examID);
	// 		//this.updateStatusSeat(this.remain_seat);
	// 		this.cdr.detectChanges();

	// 	} else if (this.ExamData.status_seat_id === 1 && this.remain_seat === 0) {
	// 		//unlimited candidate
	// 		this.update(id, i, 'unlimit');
	// 	}
	// }

	// update(id, i, verify) {
	// 	const _title: string = 'การยืนยัน';
	// 	const _description: string = 'ต้องการเปลี่ยนสถานะการชำระเงิน ใช่หรือไม่ ?';
	// 	const _waitDesciption: string = 'กำลังดำเนินการ...';

	// 	const dialogRef = this.layoutUtilsService.modalElement(_title, _description, _waitDesciption);
	// 	dialogRef.afterClosed().subscribe(res => {
	// 		if (!res) {
	// 			this.list(this.examID)
	// 			return false;
	// 		}
	// 		else {
	// 			if (this.ExamData.seat > 0 && this.ExamData.status_seat_id === 2 && this.remain_seat === 0) {
	// 				this.ExamData.status_seat_id = 1;
	// 				this.examService.update(this.examID, this.ExamData).then((res) => {
	// 					// this.list(this.examID)
	// 				})
	// 					.catch((error) => {
	// 						this.layoutUtilsService.showActionNotification(error.error, MessageType.Read)
	// 					})
	// 			}
	// 			let reg_confirmed_data = this.dataSource.data[i].is_reg_confirmed;
	// 			// check if is_reg_confirmed is confirmed(1), change it into paid pending (0)
	// 			if (reg_confirmed_data === this.confirm) {
	// 				this.dataSource.data[i].is_reg_confirmed = this.pending;
	// 			}
	// 			// check if is_reg_confirmed is paid pending(0), change it into confirmed(1)
	// 			else if (reg_confirmed_data === this.pending) {
	// 				this.dataSource.data[i].is_reg_confirmed = this.confirm;
	// 			} else {
	// 				this.layoutUtilsService.showActionNotification("ระบบเกิดข้อผิดพลาด", MessageType.Read);
	// 			}
	// 			this.userExamService.update(id, this.dataSource.data[i]).then((res) => {
	// 				if (this.ExamData.status_seat_id === 1 && this.remain_seat > 0 && verify === 'limit') {
	// 					this.updateStatusSeat(this.remain_seat)
	// 				}
	// 				this.layoutUtilsService.showActionNotification("บันทึกสำเร็จ", MessageType.Create)
	// 				this.list(this.examID)
	// 				return;
	// 			})
	// 				.catch((error) => {
	// 					this.layoutUtilsService.showActionNotification(error.error, MessageType.Read)
	// 				})
	// 		}
	// 	})
	// }

	// updateStatusSeat(remain_seat) {
	// 	remain_seat = this.ExamData.seat - this.taken_seat - 1;
	// 	if (remain_seat === 0) {
	// 		this.ExamData.status_seat_id = 2;
	// 		this.examService.update(this.examID, this.ExamData).then((res) => {
	// 			this.layoutUtilsService.showActionNotification("ที่นังสุดทายถูกเลือกแล้ว", MessageType.Update)
	// 			this.cdr.detectChanges();
	// 			this.list(this.examID)
	// 			return;
	// 		})
	// 			.catch((error) => {
	// 				this.layoutUtilsService.showActionNotification(error.error, MessageType.Read)
	// 			})
	// 	}
	// }

	initSubHeader() {
		this.SubheaderService.setTitle('รายการสมัครสอบ');
		this.SubheaderService.setBreadcrumbs([
			{ title: 'รายการสมัครสอบ', page: "/exam-data" },
			{ title: 'ผู้สมัคร' ,page:'./' }
		]);
		this.getComponentTitle();
	}

	getComponentTitle() {
		let result = `ผู้สมัครของรายการ : `;
		return result;
	}

	initFormList() {
		this.dataSource.paginator = this.paginator;
		this.dataSource.sort = this.sort;
	}

	showConfirmDialog(reg_confirmed, user_exam_id, i) {
		if(reg_confirmed == 3){
			const _title: string = 'การสมัครไม่สำเร็จ';
			const _description: string = 'สาเหตุ';
			const _waitDesciption: string = 'กำลังดำเนินการ...';
			const dialogRef = this.layoutUtilsService.paymentDeny(_title, _description, _waitDesciption, user_exam_id);
			dialogRef.afterClosed().subscribe(res => {
				if (!res) {
					this.list(this.examID)
					return;
				}else{
					this.list(this.examID)
				}
			});
		}
		else {
			const _title: string = 'การยืนยัน';
			const _description: string = 'ต้องการเปลี่ยนสถานะการชำระเงิน ใช่หรือไม่ ?';
			const _waitDesciption: string = 'กำลังดำเนินการ...';
			const dialogRef = this.layoutUtilsService.modalElement(_title, _description, _waitDesciption);
			dialogRef.afterClosed().subscribe(res => {
				if (!res) {
					this.list(this.examID)
					return;
				}
				let data = {
					is_reg_confirmed: reg_confirmed,
					seat_no: this.seatNumberGenerator(reg_confirmed,i),
					description: null
				}

				this.userExamService.update(user_exam_id, data).then((res) => {
					this.list(this.examID)
					this.layoutUtilsService.showActionNotification("บันทึกสำเร็จ", MessageType.Read)
				}).catch((error) => {
					this.layoutUtilsService.showActionNotification(error.error, MessageType.Read)
				})
			});
		}
	}
	seatNumberGenerator(reg_confirmed,i){
		// seat number generator
		if(reg_confirmed == 4){
			// Seat number is generated when reg_confirmed is confirmed
			for (let j = 1; j <= this.dataSource.data.length; j++) {
				let k = j;
				if (this.dataSource.data.find(fruit => fruit.seat_no === k.toString()) == null) {
					this.dataSource.data[i].seat_no = k.toString();
					break;
				}
			}
		}else{
			// remove seat number when reg_confirmed is denied
			this.dataSource.data[i].seat_no = null;
		}
		return this.dataSource.data[i].seat_no;
	}
}

