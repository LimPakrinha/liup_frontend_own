import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';

// Material
import {
	MatInputModule,
	MatPaginatorModule,
	MatProgressSpinnerModule,
	MatSortModule,
	MatTableModule,
	MatSelectModule,
	MatMenuModule,
	MatProgressBarModule,
	MatButtonModule,
	MatCheckboxModule,
	MatDialogModule,
	MatTabsModule,
	MatNativeDateModule,
	MatCardModule,
	MatRadioModule,
	MatIconModule,
	MatDatepickerModule,
	MatAutocompleteModule,
	MAT_DIALOG_DEFAULT_OPTIONS,
	MatSnackBarModule,
	MatTooltipModule
} from '@angular/material';



import { UserClientListComponent } from './user-client/user-client-list/user-client-list.component';
import { UserClientFormComponent } from './user-client/user-client-form/user-client-form.component';
import { UserAdminFormComponent } from './user-admin/user-admin-form/user-admin-form.component';
import { UserAdminComponent } from './user-admin/user-admin.component';
import { UserAdminUpdateComponent } from './user-admin/user-admin-update/user-admin-update.component';
import { UserAdminPasswordFormComponent } from './user-admin/user-admin-update/user-admin-password-form/user-admin-password-form.component';
import { ThaiDatePipe } from '../../../../core/pipes/thai-date.pipe';
import { CoreModule } from '../../../../core/core.module';


const routes: Routes = [
	
	{
		path: 'user-admin/add',
		component: UserAdminFormComponent,
	},
	{
		path: 'user-admin/edit',
		component: UserAdminUpdateComponent,
	},
	{
		path: 'user-admin',
		component: UserAdminComponent,
	},
	{
		path: 'client',
		component: UserClientListComponent,
	},
	{
		path: 'client/show',
		component: UserClientFormComponent,
	}
];

@NgModule({
	imports: [
		MatDialogModule,
		CommonModule,
		HttpClientModule,
		RouterModule.forChild(routes),
		FormsModule,
		ReactiveFormsModule,
		TranslateModule.forChild(),
		MatButtonModule,
		MatMenuModule,
		MatSelectModule,
		MatInputModule,
		MatTableModule,
		MatAutocompleteModule,
		MatRadioModule,
		MatIconModule,
		MatNativeDateModule,
		MatProgressBarModule,
		MatDatepickerModule,
		MatCardModule,
		MatPaginatorModule,
		MatSortModule,
		MatCheckboxModule,
		MatProgressSpinnerModule,
		MatSnackBarModule,
		MatTabsModule,
		MatTooltipModule,
		CoreModule,
	],
	providers: [
		{
			provide: MAT_DIALOG_DEFAULT_OPTIONS,
			useValue: {
				hasBackdrop: true,
				panelClass: 'm-mat-dialog-container__wrapper',
				height: 'auto',
				width: '900px'
			}
		},
	],
	entryComponents: [],
	declarations: [
		UserAdminFormComponent,
		UserAdminComponent,
		UserAdminUpdateComponent,
		UserAdminPasswordFormComponent,
		UserClientListComponent,
		UserClientFormComponent,
	]
})
export class UserModule { }
