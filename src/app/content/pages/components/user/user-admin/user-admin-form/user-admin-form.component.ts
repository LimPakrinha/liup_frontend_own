import { Component, OnInit, ChangeDetectorRef, Inject, ViewChild, ElementRef } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SubheaderService } from '../../../../../../core/services/layout/subheader.service';
import { LayoutUtilsService, MessageType } from '../../../../../../core/services/layout/layout-utils.service';
import { BehaviorSubject } from 'rxjs';
import {UserAdminService} from '../../../../../../service/user-admin/user-admin.service';
import { DateAdapter } from '@angular/material/core';
import {UserModel} from '../../../../../../core/models/user.model';
import * as moment from 'moment';
import { AuthNoticeService } from '../../../../../../core/auth/auth-notice.service';
import { AuthenticationService } from '../../../../../../core/auth/authentication.service';
import { TranslateService } from '@ngx-translate/core';
import { ErrorStateMatcher } from '@angular/material/core';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const invalidCtrl = !!(control && control.invalid && control.parent.dirty);
    const invalidParent = !!(control && control.parent && control.parent.invalid && control.parent.dirty);

    return (invalidCtrl || invalidParent);
  }
}
@Component({
  selector: "m-user-admin-form",
  templateUrl: "./user-admin-form.component.html"
})
export class UserAdminFormComponent implements OnInit {

	public model: any = { email: '' , user_type_id:'1' };
  loadingSubject = new BehaviorSubject<boolean>(false);
  loading$ = this.loadingSubject.asObservable();
  userformModel: UserModel;
  userformModelOld: UserModel;
  UserformGroup: FormGroup;
  errors: any = [];
  matcher = new MyErrorStateMatcher();

  constructor(
    
    @Inject('API_URL') private API_URL: string,
	private router: Router,
	private activatedRoute: ActivatedRoute,
	private subheaderService: SubheaderService,
    private useradminservice:UserAdminService,
    private formBuilder: FormBuilder,
    private authService: AuthenticationService,
    public authNoticeService: AuthNoticeService,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
    private changeDetectorRef: ChangeDetectorRef,
    private layoutUtilsService: LayoutUtilsService,
  ) { this.createForm() }
	

  ngOnInit() {
    this.createForm();
		this.loadingSubject.next(true);
		this.activatedRoute.queryParams.subscribe(params => {
			const id = + params.id;
			
			if (id && id > 0) {
				this.useradminservice.get(id).then(res => {
					this.userformModel = res.data;
					// console.log(this.userformModel,"first stage")
					this.userformModelOld = Object.assign({}, res.data);
					// console.log(this.userformModelOld,"second stage")
					this.initPage();
					this.initSubHeader()
				});
			} else {
				const newModel = new UserModel();
				newModel.clear();
				this.userformModel = newModel;
				console.log(this.userformModel)
				this.userformModelOld = Object.assign({}, newModel);
				this.initSubHeader()
			}
		});
  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
  let pass = group.controls.password.value;
  let confirmPass = group.controls.confirm_password.value;

  return pass === confirmPass ? null : { notSame: true }
}

  goBack(id = 0) {
    let _backUrl = "user/user-admin";
    if (id > 0) {
      _backUrl += "?id=" + id;
    }
    this.router.navigateByUrl(_backUrl);
  }

  createForm() {
	if (this.userformModel) {
		this.UserformGroup = this.formBuilder.group({
        	card_no: [this.userformModel.card_no, [Validators.required, Validators.maxLength(255)]],
			first_name: [this.userformModel.first_name, [Validators.required, Validators.maxLength(255)]],
			last_name: [this.userformModel.last_name, [Validators.required, Validators.maxLength(255)]],
			email: [this.userformModel.email, [Validators.required, Validators.maxLength(255)]],
			password: [this.userformModel.password, [Validators.required, Validators.minLength(4)]],
			confirm_password: [this.userformModel.password, [Validators.required, Validators.minLength(4)]],
		},{ validator: this.checkPasswords });

			this.changeDetectorRef.detectChanges();
			
	}else{
		this.UserformGroup = this.formBuilder.group({
			card_no: ["", [Validators.required, Validators.maxLength(255)]],
			first_name: ["", [Validators.required]],
			last_name: ["", [Validators.required]],
			email: ["", [Validators.required]],
			password: ["", [Validators.required,Validators.minLength(4)]],
			confirm_password: ["", [Validators.required,Validators.minLength(4)]],
		},{ validator: this.checkPasswords });

	}
	console.log(this.UserformGroup,"create form")

  }

  onSumbit(withBack: boolean = false) {
	  
		const controls = this.UserformGroup.controls;
		console.log(controls,"control")
		// tslint:disable-next-line:prefer-const
    	let editedData = this.prepareUser();
		/** check form */
		console.log(this.UserformGroup.invalid,"form invalid")
		if (this.UserformGroup.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}
		
		if (editedData.id > 0) {
			this.update(editedData, withBack);

			return;
		}
		this.add(editedData, withBack);
  }

  prepareUser(): UserModel {
		const controls = this.UserformGroup.controls;
		const _formValue = new UserModel();

		console.log(_formValue,"_formValue")

		// _formValue.id = this.userformModel.id;
		_formValue.card_no = controls['card_no'].value;
		_formValue.user_type_id = 1;
		_formValue.first_name = controls['first_name'].value;
		_formValue.last_name = controls['last_name'].value;
		_formValue.email = controls['email'].value;
		_formValue.password = controls['password'].value;

		console.log("new form")
		console.log(_formValue,"_formValue")
		return _formValue;
	}

  initPage() {
		this.createForm();
		this.loadingSubject.next(false);
	}

  initSubHeader() {
		this.subheaderService.setTitle('ผู้ดูแล');
		this.subheaderService.setBreadcrumbs([
			{title:'เพิ่มผู้ดูแล'},
		//   { title: 'ผู้ดูแล'}
    ]);
    this.getComponentTitle();
	  }
	
	  getComponentTitle() {
		let result = `ผู้ดูแล`;
		return result;
    }

    update(_formValue: UserModel, withBack: boolean = false) {
      this.useradminservice.update(_formValue.id, _formValue)
        .then((res) => {
          this.layoutUtilsService.showActionNotification("บันทึกสำเร็จ", MessageType.Update)
          this.goBack()
        })
        .catch((error) => {
          this.layoutUtilsService.showActionNotification(error.error, MessageType.Read)
        })
    }
    add(_formValue: UserModel, withBack: boolean = false) {
		console.log("after senddd")
	  console.log(_formValue)
      this.authService.register(_formValue).subscribe(response => {
				
		if (response.status === 'success') {
          this.layoutUtilsService.showActionNotification("บันทึกสำเร็จ", MessageType.Create)
          this.goBack()
				}
		else{
		  this.errors = [];
			if(response.error.card_no){
				  this.errors.push("บันทึกไม่สำเร็จ ชื่อบัญชีผู้ใช้งานถูกเลือกแล้ว");
			}
			if(response.error.email){
				  this.errors.push("บันทึกไม่สำเร็จ อีเมลถูกเลือกแล้ว");
			  }
		this.layoutUtilsService.showActionNotification(this.errors.join('<br/>'), MessageType.Read)
			}
				
			});
    }
    
}
