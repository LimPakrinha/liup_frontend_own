import { ChangeDetectionStrategy, Component, OnInit, ViewChild, ElementRef, HostListener, ChangeDetectorRef, Output, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { SubheaderService } from '../../../../../core/services/layout/subheader.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { UserAdminService } from '../../../../../service/user-admin/user-admin.service';
import { LayoutUtilsService, MessageType } from '../../../../../core/services/layout/layout-utils.service';
import { UserModel } from '../../../../../core/models/user.model';
import { UserAdminDataSource } from '../../../../../core/data-sources/user-admin.datasource';
import { fromEvent, merge } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { QueryParamsModel } from '../../../../../core/models/query-params.model';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'm-user-admin',
  templateUrl: './user-admin.component.html',
  styleUrls: ['./user-admin.component.scss']
})
export class UserAdminComponent implements OnInit {
  
  dataSource: UserAdminDataSource;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

	// Filter fields
	@ViewChild('searchInput') searchInput: ElementRef;
	filterStatus: string = '';
	filterCondition: string = '';
	// Selection
	selection = new SelectionModel<UserModel>(true, []);
	dataResult: UserModel[] = [];



  displayedColumns = ['username', 'fullname', 'email', 'action']
  private userlogin:any

  constructor(
    private route: ActivatedRoute,
		private subheaderService: SubheaderService,
    private cdr: ChangeDetectorRef,
    private useradminservice :UserAdminService,
    private layoutUtilsService: LayoutUtilsService,
  ) { this.userlogin = JSON.parse(localStorage.getItem('userlogin')); }

  ngOnInit() {
    this.initSubHeader()
    this.getComponentTitle()

//----------------

// If the user changes the sort order, reset back to the first page.
this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

/* Data load will be triggered in two cases:
- when a pagination event occurs => this.paginator.page
- when a sort event occurs => this.sort.sortChange
**/
merge(this.sort.sortChange, this.paginator.page)
  .pipe(
    tap(() => {
      this.loadList();
    })
  )
  .subscribe();

// Filtration, bind to searchInput
fromEvent(this.searchInput.nativeElement, 'keyup')
  .pipe(
    debounceTime(150),
    distinctUntilChanged(),
    tap(() => {
      this.paginator.pageIndex = 0;
      this.loadList();
    })
  )
  .subscribe();

// Set title to page breadCrumbs
// Init DataSource
this.dataSource = new UserAdminDataSource(this.useradminservice);

let queryParams = new QueryParamsModel({});
// Read from URL itemId, for restore previous state
this.route.queryParams.subscribe(params => {
  if (params.id) {
    // queryParams = this.userService.lastFilter$.getValue();
    // this.restoreState(queryParams, +params.id);
  }
  // First load
  this.dataSource.loadData(this.userlogin.id, queryParams);
  console.log(this.dataSource)
});
this.dataSource.entitySubject.subscribe(res => this.dataResult = res);

  }

  // getAllUser()
	// {	
	// 	this.useradminservice.getAllUser(this.userlogin.id).subscribe((res) =>{
	// 		this.dataSource.data =res.data;
	// 		// this.initFormList();
	// 	})
	// }

	loadList() {
		this.selection.clear();
		const queryParams = new QueryParamsModel(
			this.filterConfiguration(),
			this.sort.direction,
			this.sort.active,
			this.paginator.pageIndex,
			this.paginator.pageSize
		);
		this.dataSource.loadData(this.userlogin.id, queryParams);
  }
  
  initSubHeader() {
		this.subheaderService.setTitle('ผู้ดูแล');
		this.subheaderService.setBreadcrumbs([
			{ title: 'ผู้ดูแล' }
		]);
  }
  
  getComponentTitle() {
		let result = 'ผู้ดูแล';
		return result;
  }
  
  delete(id :number) {
    const _title: string = 'การยืนยันลบรายการ';
    const _description: string = 'ต้องการลบรายการนี้ ใช่หรือไม่ ?';
    const _waitDesciption: string = 'กำลังดำเนินการลบ...';
  
    const dialogRef = this.layoutUtilsService.deleteElement(_title, _description, _waitDesciption);
    dialogRef.afterClosed().subscribe(res => {
      if (!res) {
        alert("mo")
      return;
      }
      else{
      this.useradminservice.delete(id)
        .then((response) => {
          console.log(response)
        if (response.status === "success") {
          this.layoutUtilsService.showActionNotification("บัญขีถูกลบออกจากระบบ", MessageType.Read)
          this.loadList();
        }
        })
        .catch(error => {
        if(error.error.title){
          this.layoutUtilsService.showActionNotification(error.error.title, MessageType.Read)
        }else{
          this.layoutUtilsService.showActionNotification("System error", MessageType.Read)
        }
        this.loadList();
        });
      }
    })
    }

	/** FILTRATION */
	filterConfiguration(): any {
		const filter: any = {};
		const searchText: string = this.searchInput.nativeElement.value;
		filter['user.first_name'] = searchText;
		filter['user.last_name'] = searchText;
		return filter;
	}

	/** SELECTION */
	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataResult.length;
		return numSelected === numRows;
	}

	/** Selects all rows if they are not all selected; otherwise clear selection. */
	masterToggle() {
		if (this.isAllSelected()) {
			this.selection.clear();
		} else {
			this.dataResult.forEach(row => this.selection.select(row));
		}
	} 
}
