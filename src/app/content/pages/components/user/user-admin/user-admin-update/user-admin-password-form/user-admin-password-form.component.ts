import { Component, OnInit, OnDestroy, ViewChild, ChangeDetectorRef } from '@angular/core';
import { UserModel } from '../../../../../../../core/models/user.model';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AuthenticationService } from '../../../../../../../core/auth/authentication.service';
import { PagesNoticeService } from '../../../../../../../core/auth/pages-notice.service';
import { PasswordValidation } from '../../../../../../../core/validator/custom-validator';
import { LayoutUtilsService, MessageType } from '../../../../../../../core/services/layout/layout-utils.service';
import { UserAdminService } from '../../../../../../../service/user-admin/user-admin.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../../../../../service/user/user.service';
import { ErrorStateMatcher } from '@angular/material/core';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const invalidCtrl = !!(control && control.invalid && control.parent.dirty);
    const invalidParent = !!(control && control.parent && control.parent.invalid && control.parent.dirty);

    return (invalidCtrl || invalidParent);
  }
}

@Component({
  selector: 'm-user-admin-password-form',
  templateUrl: './user-admin-password-form.component.html',
  styleUrls: ['./user-admin-password-form.component.scss']
})
export class UserAdminPasswordFormComponent implements OnInit {

  user_id: number;
  userModel: UserModel;
  userPasswordformGroup: FormGroup;
  private currentUserFrom_subscription: Subscription;
  matcher = new MyErrorStateMatcher();

  @ViewChild('form') form;
  
  constructor(
    private athenticationService: AuthenticationService,
    private changeDetectorRef: ChangeDetectorRef,
    private formBuilder: FormBuilder,
    private layoutUtilsService: LayoutUtilsService,
    private pagesNoticeService: PagesNoticeService,
    private useradminService: UserAdminService,
    private activatedRoute:ActivatedRoute,
    private userService:UserService,
    private router:Router,
    // private passwordvalidate:PasswordValidation,
  ) {
    this.initForm()
  }

  

  ngOnInit() {
        this.pagesNoticeService.setNotice(null);
        this.initForm()
    this.activatedRoute.queryParams.subscribe(params => {
			const id = + params.id;
        this.userService.getUserById(id).subscribe(response => {
        this.userModel =response.data
        this.user_id =  id
      })
    })
  }
  
  initForm(){
		if (this.userModel) {
      this.userPasswordformGroup = this.formBuilder.group({
        password: ['', [Validators.required, Validators.minLength(4)]],
				confirm_password: ['', [Validators.required, Validators.minLength(4)]]
      }, { validator: this.checkPasswords });
			// this.changeDetectorRef.detectChanges();
		} else {
		
      this.userPasswordformGroup = this.formBuilder.group({
        password: ['', [Validators.required, Validators.minLength(4)]],
				confirm_password: ['', [Validators.required, Validators.minLength(4)]],
      },{ validator: this.checkPasswords });
		}
  }
  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirm_password.value;

    return pass === confirmPass ? null : { notSame: true }
  }

  onSubmitPasswordForm(withBack: boolean = false){  
    const controls = this.userPasswordformGroup.controls;
		/** check form */
		if (this.userPasswordformGroup.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
    }
    
		// tslint:disable-next-line:prefer-const
		let userData = this.prepareUserPassword();

		if (userData.id > 0) {
			this.updateUser(userData, withBack);
			return;
    }
  }

  prepareUserPassword(): UserModel {
		const controls = this.userPasswordformGroup.controls;
		const _user = new UserModel();
    _user.id = this.user_id
    _user.card_no=this.userModel.card_no
    _user.first_name = this.userModel.first_name
    _user.last_name = this.userModel.last_name
    _user.email = this.userModel.email
    _user['password'] = controls['password'].value;
    _user['confirm_password'] = controls['confirm_password'].value;
		return _user;
	}
  
  updateUser(_user: UserModel, withBack: boolean = false){
    this.useradminService.update(_user.id, _user)
      .then((response: any)=>{
				this.layoutUtilsService.showActionNotification("บันทึกสำเร็จ", MessageType.Update)
        this.goBack()
        
			})
			.catch((error) => {
				this.layoutUtilsService.showActionNotification(error.error, MessageType.Read)
			})
  }

  goBack(id=0) {
    let _backUrl = "user/user-admin";
    if (id > 0) {
      _backUrl += "?id=" + id;
    }
    this.router.navigateByUrl(_backUrl);
  }

}
