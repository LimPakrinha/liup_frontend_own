import { Component, OnInit, ChangeDetectionStrategy, ElementRef, ViewChild, ChangeDetectorRef, Inject, OnDestroy } from '@angular/core';
import { UserModel } from '../../../../../../core/models/user.model';
import { FormBuilder, FormGroup, Validators,FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { SubheaderService } from '../../../../../../core/services/layout/subheader.service';
import { LayoutUtilsService, MessageType } from '../../../../../../core/services/layout/layout-utils.service';
// import { PasswordValidation } from '../../../../../core/validator/custom-validator';
import { PagesNoticeService } from '../../../../../../../app/core/auth/pages-notice.service';
import { UserService } from '../../../../../../service/user/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserAdminService } from '../../../../../../service/user-admin/user-admin.service';


@Component({
  selector: 'm-user-admin-update',
  templateUrl: './user-admin-update.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserAdminUpdateComponent implements OnInit {

  user_id: number;
  userModel: UserModel;
  // private userlogin: any;
  userformGroup: FormGroup;
  userPasswordformGroup: FormGroup;
  @ViewChild('fileInput') fileInput: ElementRef;
  profileImage_defualt: string = "../../../../../assets/app/media/img/profile/feedback.jpg"
  
  private currentUserFrom_subscription: Subscription;

  constructor(
    private subheaderService: SubheaderService,
    private formBuilder: FormBuilder,
    private changeDetectorRef: ChangeDetectorRef,
    private layoutUtilsService: LayoutUtilsService,
    @Inject('LIMIT_FILE_SIZE') public LIMIT_FILE_SIZE: any,
    private pagesNoticeService: PagesNoticeService,
    private userService : UserService,
    private activatedRoute:ActivatedRoute,
    private router :Router,
    private useradminservice : UserAdminService
  ) {   
    this.initForm()
    // this.userlogin = JSON.parse(localStorage.getItem('userlogin'));
   }

  ngOnInit() {
    
    // this.pagesNoticeService.setNotice(null);
    
    this.initSubHeader();
    this.initForm();
    this.getComponentTitle();
    this.getUserInfo();
  }


  initSubHeader() {
    this.subheaderService.setTitle('ผู้ดูแล');
    this.subheaderService.setBreadcrumbs([
      { title: 'แก้ไขบัญชีผู้ดูแล', page: '/profile' },
    ]);
  }
  
  getComponentTitle() {
		let result = `ผู้ดูแล`;
    return result;
  }

  initForm(){
		if (this.userModel) {
			this.userformGroup = this.formBuilder.group({
        card_no: [this.userModel.card_no, [Validators.required, Validators.maxLength(128)]],
        first_name: [this.userModel.first_name, [Validators.required, Validators.maxLength(128)]],
        last_name: [this.userModel.last_name, [Validators.required, Validators.maxLength(128)]],
        email: [this.userModel.email, [Validators.required, Validators.email]],
        // password: ['', [Validators.required, Validators.maxLength(128)]],
      })
			this.changeDetectorRef.detectChanges();
    } 
    else {
			this.userformGroup = this.formBuilder.group({
        card_no: ['', [Validators.required, Validators.maxLength(128)]],
        first_name: ['', [Validators.required, Validators.maxLength(128)]],
        last_name: ['', [Validators.required, Validators.maxLength(128)]],
        email: ['', [Validators.required, Validators.email]],
        // password: ['', [Validators.required, Validators.maxLength(128)]],
      });
    }
  }

  onSumbit(withBack: boolean = false) {
	  
		const controls = this.userformGroup.controls;
		console.log(controls,"control")
		// tslint:disable-next-line:prefer-const
    	let editedData = this.prepareUser();
		/** check form */
		console.log(this.userformGroup.invalid,"form invalid")
		if (this.userformGroup.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}
		
		if (editedData.id > 0) {
			this.update(editedData, withBack);

			return;
		}
		// this.add(editedData, withBack);
  }
  

  prepareUser(): UserModel {
		const controls = this.userformGroup.controls;
		const _user = new UserModel();
    _user.id = this.user_id
    _user.card_no= controls['card_no'].value;
    _user.first_name = controls['first_name'].value;
    _user.last_name = controls['last_name'].value;
    _user.email = controls['email'].value;
    // _user.password = controls['password'].value;
		return _user;
  }
  
  update(_user: UserModel, withBack: boolean = false) {
    console.log("hiooo")
    console.log(_user.id)
    console.log(_user)
    this.useradminservice.update(_user.id, _user)
			.then((res) => {
        console.log(res)
				this.layoutUtilsService.showActionNotification("บันทึกสำเร็จ", MessageType.Update)
				this.goBack()
			})
			.catch((error) => {
				this.layoutUtilsService.showActionNotification(error.error, MessageType.Read)
			})
	}

  goBack(id=0) {
        let _backUrl = "user/user-admin";
        if (id > 0) {
          _backUrl += "?id=" + id;
        }
        this.router.navigateByUrl(_backUrl);
      }

  onTabChange(event: any){
    this.initForm()
    this.getUserInfo()
  }

  getUserInfo(){
    this.activatedRoute.queryParams.subscribe(params => {
			const id = + params.id;

    this.userService.getUserById(id).subscribe(response => {
        this.userModel =response.data
        this.user_id =  id
        this.initForm()
      })
    })
  }

}


