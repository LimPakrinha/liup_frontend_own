import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserAdminPasswordFormComponent } from './user-admin-password-form.component';

describe('UserAdminPasswordFormComponent', () => {
  let component: UserAdminPasswordFormComponent;
  let fixture: ComponentFixture<UserAdminPasswordFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserAdminPasswordFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAdminPasswordFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
