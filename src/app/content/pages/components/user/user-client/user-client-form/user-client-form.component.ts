import { Component, OnInit, ChangeDetectorRef, Inject, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SubheaderService } from '../../../../../../core/services/layout/subheader.service';
import { LayoutUtilsService, MessageType } from '../../../../../../core/services/layout/layout-utils.service';
import { BehaviorSubject } from 'rxjs';
import { UserModel } from '../../../../../../core/models/user.model';
import { UserClientService } from '../../../../../../service/user-client/user-client.service';
@Component({
  selector: "m-user-client-form",
  templateUrl: "./user-client-form.component.html"
})
export class UserClientFormComponent implements OnInit {
  UserClientformModel: UserModel;
  UserClientformModelOld: UserModel;
  loadingSubject = new BehaviorSubject<boolean>(false);
  loading$ = this.loadingSubject.asObservable();
  UserClientFormGroup: FormGroup;
  constructor(
    @Inject('API_URL') private API_URL: string,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private subheaderService: SubheaderService,
    private formBuilder: FormBuilder,
    private changeDetectorRef: ChangeDetectorRef,
    private userClientService: UserClientService,
    private layoutUtilsService: LayoutUtilsService,
  ) { }


  ngOnInit() {
    this.createForm();
    this.loadingSubject.next(true);
    this.activatedRoute.queryParams.subscribe(params => {
      const id = + params.id;

      if (id && id > 0) {
        this.userClientService.get(id).then(res => {
          this.UserClientformModel = res.data;
          this.UserClientformModelOld = Object.assign({}, res.data);
          this.initPage();
          this.initSubHeader()
        });
      } else {
        const newModel = new UserModel();
        newModel.clear();
        this.UserClientformModel = newModel;
        this.UserClientformModelOld = Object.assign({}, newModel);
        this.initSubHeader()
      }
    });
  }
  createForm() {
    if (this.UserClientformModel) {
      this.UserClientFormGroup = this.formBuilder.group({
        card_type: [this.UserClientformModel.card_type_name],
        card_no: [this.UserClientformModel.card_no],
        app_type: [this.UserClientformModel.app_type_name],
        stu_id: [this.UserClientformModel.stu_id],
        faculty: [this.UserClientformModel.faculty_name],
        department: [this.UserClientformModel.depart_name],
        level: [this.UserClientformModel.level_name],
        title: [this.UserClientformModel.title_name?this.UserClientformModel.title_name:'-'],
        fname: [this.UserClientformModel.first_name],
        lname: [this.UserClientformModel.last_name],
        email: [this.UserClientformModel.email],
        phone_number: [this.UserClientformModel.phone_number],
        address: [this.UserClientformModel.address],
        province: [this.UserClientformModel.province?this.UserClientformModel.province:'-'],
        district: [this.UserClientformModel.district?this.UserClientformModel.district:'-'],
        sub_district: [this.UserClientformModel.sub_district?this.UserClientformModel.sub_district:'-'],
        zipcode: [this.UserClientformModel.zipcode?this.UserClientformModel.zipcode:'-']
      });

      this.changeDetectorRef.detectChanges();

    } else {
      this.UserClientFormGroup = this.formBuilder.group({
        card_type: [""],
        card_no: [""],
        app_type: [""],
        stu_id: [""],
        faculty: [""],
        department: [""],
        level: [""],
        title: [""],
        fname: [""],
        lname: [""],
        email: [""],
        phone_number: [""],
        address: [""],
        province: [""],
        district: [""],
        sub_district: [""],
        zipcode: [""]
      });

    }

  }
  goBack(id = 0) {
    let _backUrl = '/user/client';
    if (id > 0) {
      _backUrl += '?id=' + id;
    }
    this.router.navigateByUrl(_backUrl);
  }
  initPage() {
    this.createForm();
    this.loadingSubject.next(false);
  }
  initSubHeader() {
    this.subheaderService.setTitle('ผู้สมัคร');
    this.subheaderService.setBreadcrumbs([
      { title: 'ผู้สมัครสอบ', page: '/user/client' },
      { title: 'ผู้สมัคร' }
    ]);
    this.getComponentTitle();
  }
  getComponentTitle() {
    let result = `ผู้สมัคร`;
    return result;
  }

}
