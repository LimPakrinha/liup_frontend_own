import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { ActivatedRoute } from '@angular/router';
import { SubheaderService } from '../../../../../../core/services/layout/subheader.service';
import { LayoutUtilsService, MessageType } from '../../../../../../core/services/layout/layout-utils.service';
import { fromEvent, merge } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { QueryParamsModel } from '../../../../../../core/models/query-params.model';
import { UserClientDataSource } from '../../../../../../core/data-sources/user-client.datasource';
import { UserModel } from '../../../../../../core/models/user.model';
import { UserClientService } from '../../../../../../service/user-client/user-client.service';
@Component({
	selector: 'm-user-client-list',
	templateUrl: './user-client-list.component.html',
	styleUrls: ['./user-client-list.component.scss']
})
export class UserClientListComponent implements OnInit {
	dataSource: UserClientDataSource;
	displayedColumns = ['card_no', 'card_type', 'email', 'phone_number', 'first_name', 'last_name', 'actions'];
	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
	// Filter fields
	@ViewChild('searchInput') searchInput: ElementRef;
	filterStatus: string = '';
	filterCondition: string = '';
	// Selection
	selection = new SelectionModel<UserModel>(true, []);
	dataResult: UserModel[] = [];

	constructor(
		public dialog: MatDialog,
		private route: ActivatedRoute,
		private layoutUtilsService: LayoutUtilsService,
		private subheaderService: SubheaderService,
		private userClientService: UserClientService
	) { }


	ngOnInit() {
		this.initSubHeader();
		// If the user changes the sort order, reset back to the first page.
		this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

		/* Data load will be triggered in two cases:
		- when a pagination event occurs => this.paginator.page
		- when a sort event occurs => this.sort.sortChange
		**/
		merge(this.sort.sortChange, this.paginator.page)
			.pipe(
				tap(() => {
					this.loadUserClientList();
				})
			)
			.subscribe();

		// Filtration, bind to searchInput
		fromEvent(this.searchInput.nativeElement, 'keyup')
			.pipe(
				debounceTime(150),
				distinctUntilChanged(),
				tap(() => {
					this.paginator.pageIndex = 0;
					this.loadUserClientList();
				})
			)
			.subscribe();

		// Set title to page breadCrumbs
		// Init DataSource
		this.dataSource = new UserClientDataSource(this.userClientService);
		console.log(this.dataSource)
		let queryParams = new QueryParamsModel({});
		// Read from URL itemId, for restore previous state
		this.route.queryParams.subscribe(params => {
			if (params.id) {
				// queryParams = this.userService.lastFilter$.getValue();
				// this.restoreState(queryParams, +params.id);
			}
			// First load
			this.dataSource.loadData(queryParams);
		});
		this.dataSource.entitySubject.subscribe(res => this.dataResult = res);
	}

	/** FILTRATION */
	filterConfiguration(): any {
		const filter: any = {};
		const searchText: string = this.searchInput.nativeElement.value;
		filter['user.first_name'] = searchText;
		filter['user.last_name'] = searchText;
		return filter;
	}

	/** SELECTION */
	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataResult.length;
		return numSelected === numRows;
	}

	/** Selects all rows if they are not all selected; otherwise clear selection. */
	masterToggle() {
		if (this.isAllSelected()) {
			this.selection.clear();
		} else {
			this.dataResult.forEach(row => this.selection.select(row));
		}
	}
	loadUserClientList() {
		this.selection.clear();
		const queryParams = new QueryParamsModel(
			this.filterConfiguration(),
			this.sort.direction,
			this.sort.active,
			this.paginator.pageIndex,
			this.paginator.pageSize
		);
		this.dataSource.loadData(queryParams);
	}
	initSubHeader() {
		this.subheaderService.setTitle('ผู้สมัครสอบ');
		this.subheaderService.setBreadcrumbs([
			{title:'ผู้สมัครสอบ'},
		//   { title: 'ผู้ดูแล'}
    ]);
	  }
}
