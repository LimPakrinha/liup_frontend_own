import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy, Inject, ViewChild, ElementRef } from '@angular/core';
import { UserService } from '../../../../../service/user/user.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { UserModel } from '../../../../../core/models/user.model';
import { GenderService } from '../../../../../service/gender/gender.service';
import { LayoutUtilsService, MessageType } from '../../../../../core/services/layout/layout-utils.service';
import { DateAdapter } from '@angular/material/core';
import { UserClientService } from '../../../../../service/user-client/user-client.service';
import * as moment from 'moment';
// import { } from '@types/googlemaps';

@Component({
  selector: 'm-admin-profile',
  templateUrl: './admin-profile.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileAdminComponent implements OnInit, OnDestroy {
  private userlogin: any;
  public user: UserModel;
  public userForm: FormGroup;
  // private user: any = {};
  private company: any = {};
  genderData: any = [];
  hasFormErrors: boolean = false;

  constructor(
      private cdr: ChangeDetectorRef,
      private userService: UserClientService,
      private userUpdateService: UserService,
      private userFB: FormBuilder,
      private layoutUtilsService: LayoutUtilsService,
      //private genderService: GenderService,
      @Inject('API_URL') public API_URL: string,
      private dateAdapter: DateAdapter<any>
  ) {
    this.dateAdapter.setLocale('th-TH');
    this.userlogin = JSON.parse(localStorage.getItem('userlogin'));  
    this.createForm();
  }

  ngOnInit() {
    this.getUserInfo();
  }

  ngOnDestroy() {

  }

  createForm() {
    if (this.user) {
      this.userForm = this.userFB.group({
        card_no:[this.user.card_no,[Validators.required]],
        fname: [this.user.first_name,[Validators.required]],
        lname: [this.user.last_name,[Validators.required]],
        email: [this.user.email,[Validators.required]]
      });

      this.cdr.detectChanges();

    } else {
      this.userForm = this.userFB.group({
        card_no:["",[Validators.required]],
        fname: ["",[Validators.required]],
        lname: ["",[Validators.required]],
        email: ["",[Validators.required]]
      });

    }

  }

  onSumbit(withBack: boolean = false) {
    this.hasFormErrors = false;
    const controls = this.userForm.controls;
    /** check form */

    let editedData = this.prepareUser();

    if (this.userForm.invalid) {
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      this.hasFormErrors = true;
      return;
    }

		if (editedData.id > 0) {
			this.updateUser(editedData, withBack);

			return;
		}

  }

  updateUser(_user: UserModel, withBack: boolean = false) {

    this.userUpdateService.update(_user.id, _user)
      .then((res) => {
        this.layoutUtilsService.showActionNotification("บันทึกสำเร็จ", MessageType.Update)
        this.cdr.detectChanges();
      })
      .catch((error) => {
        let msgerror=error;
        if(error.error.email){
          msgerror="อีเมลนี้ถูกเลือกแล้ว";
        }
          this.layoutUtilsService.showActionNotification(msgerror, MessageType.Read)
      })
  }

  onAlertClose($event) {
    this.hasFormErrors = false;
  }

  prepareUser(): UserModel {
    const controls = this.userForm.controls;
    const _user = new UserModel();
    _user.id = this.user.id;
    _user.last_name = controls['lname'].value;
    _user.first_name = controls['fname'].value;
    _user.email = controls['email'].value;

    return _user;
  }

  getUserInfo() {
    this.userService.getOwnAdminById(this.userlogin.id).then(res => {
      if (res.status == 'success') {
        this.user = res.data;
        console.log(this.user)
        this.createForm();
      } else {
        this.layoutUtilsService.showActionNotification(res.error, MessageType.Read)
      }

      this.cdr.detectChanges();
    });
  }

  // getGender() {
  //   this.genderService.getAll()
  //     .subscribe((res) => {
  //       if (res.status == 'success') {
  //         this.genderData = res.data
  //       } else {
  //         this.layoutUtilsService.showActionNotification(res.error, MessageType.Read)
  //       }
  //       this.cdr.detectChanges();;
  //     })
  // }

  onTabChange(event: any){
    this.getUserInfo()
    this.createForm()
  }
}
