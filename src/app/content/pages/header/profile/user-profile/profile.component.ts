import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy, Inject, ViewChild, ElementRef } from '@angular/core';
import { UserService } from '../../../../../service/user/user.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { UserModel } from '../../../../../core/models/user.model';
import { GenderService } from '../../../../../service/gender/gender.service';
import { LayoutUtilsService, MessageType } from '../../../../../core/services/layout/layout-utils.service';
import { DateAdapter } from '@angular/material/core';
import {ProvinceService} from '../../../../../service/province/province.service';
import {DistrictService} from '../../../../../service/district/district.service';
import {SubDistrictService} from '../../../../../service/subdistrict/subdistrict.service';
import {ZipcodeService} from '../../../../../service/zipcode/zipcode.service';
import { UserClientService } from '../../../../../service/user-client/user-client.service';
import {CardTypeService} from '../../../../../service/card_type/card_type.service';
import {applicanttypesService} from '../../../../../service/applicant_types/applicant_types.service';
import {TitleService} from '../../../../../service/title/title.service';
import {FacultyService} from '../../../../../service/faculty/faculty.service';
import {DepartmentService} from '../../../../../service/department/department.service';
import {LevelService} from '../../../../../service/level/level.service';
import * as moment from 'moment';

// import { } from '@types/googlemaps';

@Component({
  selector: 'm-profile',
  templateUrl: './profile.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileUserComponent implements OnInit, OnDestroy {
  private userlogin: any;
  public user: UserModel;
  public userForm: FormGroup;
  // private user: any = {};

  private company: any = {};
  genderData: any = [];
  hasFormErrors: boolean = false;
  OtherTitle: string;
  ProvinceData=[];
	DistrictData=[];
  SubdistrictData=[];
  ZipcodeData=[];
  cardtypesData= [];
  apptypesData= [];
  titleData = [];
  facultyData= [];
  departmentData = [];
  levelData = [];
  faculty_id: number;
  constructor(
      private cdr: ChangeDetectorRef,
      private userService: UserClientService,
      private userUpdateService: UserService,
      private userFB: FormBuilder,
      private layoutUtilsService: LayoutUtilsService,
      // private genderService: GenderService,
      @Inject('API_URL') public API_URL: string,
      private dateAdapter: DateAdapter<any>,
      private provinceService: ProvinceService,
      private districtService: DistrictService,
      private subdistrictService: SubDistrictService,
      private zipcodeService: ZipcodeService,
      private titleService: TitleService,
      private cardtypeService: CardTypeService,
      private ApplicanttypesService: applicanttypesService,
      private facultyService: FacultyService,
      private departmentService: DepartmentService,
      private levelService: LevelService
  ) {
    this.dateAdapter.setLocale('th-TH');
    this.userlogin = JSON.parse(localStorage.getItem('userlogin'));  
    this.createForm();
    this.departmentService.getFacultyID(this.userlogin.department_id).then(res =>{
      this.faculty_id = res.data.faculty_id;
      this.getcardtype();
    })
    
  }

  ngOnInit() {

  }

  ngOnDestroy() {

  }

  createForm() {
    if (this.user) {
      this.userForm = this.userFB.group({
        card_type: [this.user.card_type_id,[Validators.required]],
        card_no: [this.user.card_no,[Validators.required]],
        app_type: [this.user.app_type_id,[Validators.required]],
        stu_id:[this.user.stu_id],
        title: [this.user.title_id,[Validators.required]],
        other_title: [this.user.other_title],
        faculty_id: [this.user.faculty_id,[Validators.required]],
        depart_id: [this.user.department_id,[Validators.required]],
        level_id: [this.user.level_id,[Validators.required]],
        fname: [this.user.first_name,[Validators.required]],
        lname: [this.user.last_name,[Validators.required]],
        email: [this.user.email,[Validators.required]],
        phone_number: [this.user.phone_number,[Validators.required]],
        address: [this.user.address,[Validators.required]],
        province: [this.user.prov_id,[]],
        district: [this.user.dis_id,[]],
        sub_district: [this.user.sub_district_id,[]],
        zipcode: [this.user.zipcode,[]]
      });

      this.cdr.detectChanges();

    } else {
      this.userForm = this.userFB.group({
        card_type: ["",[Validators.required]],
        card_no: ["",[Validators.required]],
        app_type: ["",[Validators.required]],
        stu_id:["",[Validators.required]],
        faculty_id:["",[Validators.required]],
        depart_id:["",[Validators.required]],
        level_id:["",[Validators.required]],
        title: ["",[Validators.required]],
        fname: ["",[Validators.required]],
        lname: ["",[Validators.required]],
        email: ["",[Validators.required]],
        phone_number: ["",[Validators.required]],
        address: ["",[Validators.required]],
        province: ["",[]],
        district: ["",[]],
        sub_district: ["",[]],
        zipcode: ["",[]]
      });

    }

  }

  onSumbit(withBack: boolean = false) {
    this.hasFormErrors = false;
    const controls = this.userForm.controls;
    /** check form */

    let editedData = this.prepareUser();

    if (this.userForm.invalid) {
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      this.hasFormErrors = true;
      return;
    }

		if (editedData.id > 0) {
			this.updateUser(editedData, withBack);

			return;
		}

  }

  updateUser(_user: UserModel, withBack: boolean = false) {

    this.userUpdateService.update(_user.id, _user)
      .then((res) => {
        this.layoutUtilsService.showActionNotification("บันทึกสำเร็จ", MessageType.Update)
        this.cdr.detectChanges();
      })
      .catch((error) => {
        let msgerror=error;
        if(error.error.email){
          msgerror="อีเมลนี้ถูกเลือกแล้ว";
        }
          this.layoutUtilsService.showActionNotification(msgerror, MessageType.Read)
      })
  }

  onAlertClose($event) {
    this.hasFormErrors = false;
  }

  prepareUser(): UserModel {
    const controls = this.userForm.controls;
    const _user = new UserModel();
    _user.id = this.user.id;
    _user.app_type_id = controls['app_type'].value;
    if(controls['app_type'].value == 2){
      _user.department_id=controls['depart_id'].value;
      _user.level_id=controls['level_id'].value;
    }
    _user.title_id=controls['title'].value;
    _user.last_name = controls['lname'].value;
    _user.first_name = controls['fname'].value;
    _user.email = controls['email'].value;
    _user.phone_number = controls['phone_number'].value;
    _user.address = controls['address'].value;
    if(controls['card_type'].value == 1 )
      _user.sub_district_id = controls['sub_district'].value;
    console.log(_user);
    return _user;
  }

  getUserInfo() {
    this.userService.get(this.userlogin.id).then(res => {
      if (res.status == 'success') {
        this.user = res.data;
        this.OtherTitle=this.user.other_title;
        
      } else {
        this.layoutUtilsService.showActionNotification(res.error, MessageType.Read)
      }
      if(this.userlogin.card_type_id == 1){
          this.getPro();
        }
      if(this.user){

        this.createForm();
      }
      this.cdr.detectChanges();
    });


  }

  // getGender() {
  //   this.genderService.getAll()
  //     .subscribe((res) => {
  //       if (res.status == 'success') {
  //         this.genderData = res.data
  //       } else {
  //         this.layoutUtilsService.showActionNotification(res.error, MessageType.Read)
  //       }
  //       this.cdr.detectChanges();;
  //     })
  // }

	getPro(){
		this.provinceService.getAll().subscribe(res => {
      this.ProvinceData = res.data;	
      this.getDis(this.user.prov_id);
		})
  }
  
	getDis(pid){
		this.districtService.getDistrict(pid).subscribe(res => {
      this.DistrictData = res.data;
      this.getSubDis(this.user.dis_id);
      
		})
	}

	getSubDis(did){
	
		this.subdistrictService.getSubDistrict(did).subscribe(res => {
      this.SubdistrictData = res.data;
      this.getZipcode(this.user.sub_district_id);
      this.cdr.detectChanges();
		})
  }
  getZipcode(sid){
		this.zipcodeService.getZip(sid).subscribe(res => {
      this.ZipcodeData = res.data[0].zipcode;
      this.cdr.detectChanges();
      
		})
	}
  onTabChange(event: any){
    this.getUserInfo()
    this.createForm()
  }
  getcardtype(){
		this.cardtypeService.getAll().subscribe(res => {
      this.cardtypesData = res.data;
      this.getapplicationtype();
		})
		
  }
  getTitle() {
		this.titleService.getAll().subscribe(res => {
      this.titleData = res.data;
      this.getUserInfo();
		})
  }
  getapplicationtype(){
		this.ApplicanttypesService.getAll().subscribe(res => {
      this.apptypesData = res.data;
      this.getFaculty();
		})
		
  }
  otherTitle(tid){
    if(tid===7){
      this.OtherTitle="true";
    }else{
      this.OtherTitle="false";
    }
  }

  getFaculty() {
		this.facultyService.getAll().subscribe(faculty => {
      this.facultyData = faculty.data;
      this.getDepartment(true);
    })
    this.cdr.detectChanges();
  }

  getDepartment(event){
    this.departmentService.getDepartment(this.faculty_id).subscribe(department =>{
      this.departmentData = department.data;
      this.userForm.get('depart_id').setValue(undefined);
      if(event)
        this.getLevel();
      
    })
    this.cdr.detectChanges();
  }

  getLevel(){
    this.levelService.getAll().subscribe(level=>{
      this.levelData = level.data;
      this.getTitle();
    })    
  }

}


