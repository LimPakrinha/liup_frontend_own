import { Component, OnInit, OnDestroy, ViewChild, ChangeDetectorRef } from '@angular/core';
import { UserModel } from '../../../../../core/models/user.model';
import { FormGroup, FormControl, FormGroupDirective, NgForm, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { UserService } from '../../../../../service/user/user.service';
import { ErrorStateMatcher } from '@angular/material/core';
import { AuthenticationService } from '../../../../../core/auth/authentication.service';
import { LayoutUtilsService, MessageType } from '../../../../../core/services/layout/layout-utils.service';
import { AuthNoticeService } from '../../../../../core/auth/auth-notice.service';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const invalidCtrl = !!(control && control.invalid && control.parent.dirty);
    const invalidParent = !!(control && control.parent && control.parent.invalid && control.parent.dirty);

    return (invalidCtrl || invalidParent);
  }
}

@Component({
  selector: 'm-profile-password-form',
  templateUrl: './profile-password-form.component.html',
  styleUrls: ['./profile-password-form.component.scss']
})
export class ProfilePasswordFormComponent implements OnInit, OnDestroy {
  private userlogin: any;
  public user: UserModel;
  userPasswordformGroup: FormGroup;
  matcher = new MyErrorStateMatcher();
  private currentUserFrom_subscription: Subscription;

  @ViewChild('form') form;

  constructor(
    private athenticationService: AuthenticationService,
    private cdr: ChangeDetectorRef,
    private formBuilder: FormBuilder,
    private userService: UserService,
    // private userService: UserClientService,
    private layoutUtilsService: LayoutUtilsService,
    private authNoticeService: AuthNoticeService,
  ) {
    this.userlogin = JSON.parse(localStorage.getItem('userlogin'));
    this.initForm()
  }

  ngOnInit() {
    this.authNoticeService.setNotice(null);
    this.getCurrentUser()
    this.initForm()
  }

  ngOnDestroy() {
    this.currentUserFrom_subscription.unsubscribe();
  }

  getCurrentUser(){
    this.currentUserFrom_subscription =this.userService.getUserById(this.userlogin.id).subscribe(res => {
      if (res.status == 'success') {
        this.user = res.data;
        this.initForm();
      } else {
        this.layoutUtilsService.showActionNotification(res.error, MessageType.Read)
      }

      this.cdr.detectChanges();
    });
  }

  initForm() {
    if (this.user) {
      this.userPasswordformGroup = this.formBuilder.group({
        password: ['', [Validators.required, Validators.minLength(4)]],
        confirm_password: ['', [Validators.required, Validators.minLength(4)]]
      }, { validator: this.checkPasswords });
      this.cdr.detectChanges();
    } else {
      this.userPasswordformGroup = this.formBuilder.group({
        password: ['', [Validators.required, Validators.minLength(4)]],
        confirm_password: ['', [Validators.required, Validators.minLength(4)]]
      }, { validator: this.checkPasswords });
    }
  }

  onSumbit(withBack: boolean = false) {
    const controls = this.userPasswordformGroup.controls;
    /** check form */
    if (this.userPasswordformGroup.invalid) {
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      return;
    }

    // tslint:disable-next-line:prefer-const
    let userData = this.prepareUserPassword();

    console.log(userData.id)
    if (userData.id > 0) {
      this.updatePwd(userData, withBack);

      return;
    }
  }

  prepareUserPassword(): UserModel {
    const controls = this.userPasswordformGroup.controls;
    const _user = new UserModel();
    _user.id = this.userlogin.id
    _user['password'] = controls['password'].value;
    _user['confirm_password'] = controls['confirm_password'].value;
    return _user;
  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirm_password.value;

    return pass === confirmPass ? null : { notSame: true }
  }

  updatePwd(_user: UserModel, withBack: boolean = false) {
    this.userService.updatePwd(_user.id, _user)
      .then((res) => {
        this.layoutUtilsService.showActionNotification("บันทึกสำเร็จ", MessageType.Update)
        this.cdr.detectChanges();
      })
      .catch((error) => {
        this.layoutUtilsService.showActionNotification(error.error, MessageType.Read)
      })
  }

}
