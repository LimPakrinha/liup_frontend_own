import { LayoutModule } from '../layout/layout.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { PartialsModule } from '../partials/partials.module';
import { ActionComponent } from './header/action/action.component';
import { ProfileUserComponent } from './header/profile/user-profile/profile.component';
import { ProfileAdminComponent } from './header/profile/admin-profile/admin-profile.component';
import { ProfilePasswordFormComponent } from './header/profile/profile-password-form/profile-password-form.component';
import { CoreModule } from '../../core/core.module';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ErrorPageComponent } from './snippets/error-page/error-page.component';
import { InnerComponent } from './components/inner/inner.component';
import { ExamFormComponent } from "./components/dashboard/exam-form/exam-form.component";
import { UserExamComponent } from "./components/dashboard/user-exam/user-exam.component";
import { ExamPlaceComponent } from "./components/exam-place/exam-place.component";
import { ExamScoreComponent } from "./components/exam-score/exam-score.component";
import { ExamScoreAdminComponent } from "./components/exam-score-admin/exam-score-admin.component";
import { ExamScoreAdminListComponent } from "./components/exam-score-admin/exam-score-admin-list/exam-score-admin-list.component";
import { ExamScoreAdminFormComponent } from "./components/exam-score-admin/exam-score-admin-form/exam-score-admin-form.component";
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
// Material
import {
	MatInputModule,
	MatPaginatorModule,
	MatProgressSpinnerModule,
	MatSortModule,
	MatTableModule,
	MatSelectModule,
	MatMenuModule,
	MatProgressBarModule,
	MatButtonModule,
	MatCheckboxModule,
	MatDialogModule,
	MatTabsModule,
	MatNativeDateModule,
	MatCardModule,
	MatRadioModule,
	MatIconModule,
	MatDatepickerModule,
	MatAutocompleteModule,
	MatSlideToggleModule,
	MAT_DIALOG_DEFAULT_OPTIONS,
	MatSnackBarModule,
	MatTooltipModule
} from '@angular/material';

@NgModule({
	declarations: [
		PagesComponent,
		ActionComponent,
		ProfileUserComponent,
		ProfileAdminComponent,
		ProfilePasswordFormComponent,
		ErrorPageComponent,
		InnerComponent,
		ExamFormComponent,
		UserExamComponent,
		ExamPlaceComponent,
		ExamScoreComponent,
		ExamScoreAdminComponent,
		ExamScoreAdminListComponent,
		ExamScoreAdminFormComponent
	],
	imports: [
		CommonModule,
		HttpClientModule,
		FormsModule,
		PagesRoutingModule,
		CoreModule,
		LayoutModule,
		PartialsModule,
		AngularEditorModule,
		MatButtonModule,
		MatMenuModule,
		MatSelectModule,
		MatInputModule,
		MatTableModule,
		MatAutocompleteModule,
		MatRadioModule,
		MatIconModule,
		MatNativeDateModule,
		MatProgressBarModule,
		MatDatepickerModule,
		NgxMaterialTimepickerModule.forRoot(),
		MatCardModule,
		MatPaginatorModule,
		MatSortModule,
		MatCheckboxModule,
		MatProgressSpinnerModule,
		MatSnackBarModule,
		MatTabsModule,
		MatTooltipModule,
		MatSlideToggleModule,
		ReactiveFormsModule
	],
	providers: [
		{
			provide: MAT_DIALOG_DEFAULT_OPTIONS,
			useValue: {
				hasBackdrop: true,
				panelClass: 'm-mat-dialog-container__wrapper',
				height: 'auto',
				width: '900px'
			}
		},
	]
})
export class PagesModule {
}
