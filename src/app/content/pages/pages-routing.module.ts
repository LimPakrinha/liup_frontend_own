import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagesComponent } from './pages.component';
import { ActionComponent } from './header/action/action.component';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { ProfileUserComponent } from './header/profile/user-profile/profile.component';
import { ProfileAdminComponent } from './header/profile/admin-profile/admin-profile.component';
import { ErrorPageComponent } from './snippets/error-page/error-page.component';
import { InnerComponent } from "./components/inner/inner.component";
import { ExamFormComponent } from "./components/dashboard/exam-form/exam-form.component";
import { UserExamComponent } from "./components/dashboard/user-exam/user-exam.component";
import { AdminGuard } from '../../guard/admin/admin.guard';
import { ExamPlaceComponent } from "./components/exam-place/exam-place.component";
import { ExamScoreComponent } from "./components/exam-score/exam-score.component";
import { ExamScoreAdminComponent } from "./components/exam-score-admin/exam-score-admin.component";
import { ExamScoreAdminListComponent } from "./components/exam-score-admin/exam-score-admin-list/exam-score-admin-list.component";
import { ExamScoreAdminFormComponent } from "./components/exam-score-admin/exam-score-admin-form/exam-score-admin-form.component";
// import { UserAdminComponent } from './components/user/user-admin/user-admin.component';

const routes: Routes = [
	{
		path: '',
		component: PagesComponent,
		// Remove comment to enable login
		canActivate: [NgxPermissionsGuard],
		data: {
			permissions: {
				only: ['ADMIN', 'USER'],
				except: ['GUEST'],
				redirectTo: '/login'
			}
		},
		children: [
			{
				path: '',
				loadChildren: './components/dashboard/dashboard.module#DashboardModule',
				canActivate: [AdminGuard]
			},
			{
				path: 'exam-data',
				loadChildren: './components/dashboard/dashboard.module#DashboardModule',
				canActivate: [AdminGuard]
			},

			{
				path: 'user',
				loadChildren: './components/user/user.module#UserModule',
				canActivate: [AdminGuard]
			},
			
			{
				path: 'builder',
				loadChildren: './builder/builder.module#BuilderModule'
			},
			{
				path: 'header/actions',
				component: ActionComponent
			},
			{
				path: 'exam-data/exam-form',
				component: ExamFormComponent,
				canActivate: [AdminGuard]
			},
			{
				path: 'exam-place',
				component: ExamPlaceComponent,
				// canActivate: [AdminGuard]
			},
			{
				path: 'exam-score-admin',
				component: ExamScoreAdminComponent,
				canActivate: [AdminGuard]
			},
			{
				path: 'exam-score-admin-list',
				component: ExamScoreAdminListComponent,
				canActivate: [AdminGuard]
			},
			{
				path: 'exam-score-admin-form',
				component: ExamScoreAdminFormComponent,
				canActivate: [AdminGuard]
			},
			{
				path: 'exam-score',
				component: ExamScoreComponent,
				// canActivate: [AdminGuard]
			},
			{
				path: 'exam-data/user-exam',
				component: UserExamComponent,
				canActivate: [AdminGuard]
			},
			{
				path: 'user-exam-register',
				loadChildren: './components/user-exam-register/user-exam-register.module#UserExamRegisterModule',
			},
			{
				path: 'profile-user',
				component: ProfileUserComponent
			},
			{
				path: 'profile-admin',
				component: ProfileAdminComponent,
				canActivate: [AdminGuard]
			},
			
			{
				path: 'inner',
				component: InnerComponent
			},
			// {					
			// 	path: 'user-admin',
			// 	component: UserAdminComponent
			// },
		]
	},
	{
		path: 'login',
		canActivate: [NgxPermissionsGuard],
		loadChildren: './auth/auth.module#AuthModule',
		data: {
			permissions: {
				except: 'ADMIN'
			}
		},
	},
	{
		path: '404',
		component: ErrorPageComponent
	},
	{
		path: 'error/:type',
		component: ErrorPageComponent
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class PagesRoutingModule {
}
