import { Injectable } from '@angular/core';
import { ActionNotificationComponent } from '../../../content/pages/components/share/action-natification/action-notification.component';
import { MatSnackBar, MatDialog } from '@angular/material';

import { DeleteEntityDialogComponent } from '../../../content/pages/components/share/delete-entity-dialog/delete-entity-dialog.component';
import { FetchEntityDialogComponent } from '../../../content/pages/components/share/fetch-entity-dialog/fetch-entity-dialog.component';
import { UpdateStatusDialogComponent } from '../../../content/pages/components/share/update-status-dialog/update-status-dialog.component';
import { ConfirmEntityDialogComponent } from '../../../content/pages/components/share/confirm-entity-dialog/confirm-entity-dialog.component';
import { FailPaymentDialogComponent } from '../../../content/pages/components/share/fail-payment-dialog/fail-payment-dialog.component';
import { UploadSlipDialogComponent } from '../../../content/pages/components/share/upload-slip-dialog/upload-slip-dialog.component';

export enum MessageType {
	Create,
	Read,
	Update,
	Delete
}

@Injectable()
export class LayoutUtilsService {
	constructor(private snackBar: MatSnackBar,
		private dialog: MatDialog) { }

	// SnackBar for notifications
	showActionNotification(
		message: string,
		type: MessageType = MessageType.Create,
		duration: number = 10000,
		showCloseButton: boolean = true,
		showUndoButton: boolean = false,
		undoButtonDuration: number = 3000,
		verticalPosition: 'top' | 'bottom' = 'top'
	) {
		return this.snackBar.openFromComponent(ActionNotificationComponent, {
			duration: duration,
			data: {
				message,
				snackBar: this.snackBar,
				showCloseButton: showCloseButton,
				showUndoButton: showUndoButton,
				undoButtonDuration,
				verticalPosition,
				type,
				action: 'Undo'
			},
			verticalPosition: verticalPosition
		});
	}

	// Method returns instance of MatDialog
	deleteElement(title: string = '', description: string = '', waitDesciption: string = '') {
		return this.dialog.open(DeleteEntityDialogComponent, {
			data: { title, description, waitDesciption },
			width: '440px'
		});
	}

	// Method returns instance of MatDialog
	fetchElements(_data) {
		return this.dialog.open(FetchEntityDialogComponent, {
			data: _data,
			width: '400px'
		});
	}

	// Method returns instance of MatDialog
	updateStatus(title, statuses, messages) {
		return this.dialog.open(UpdateStatusDialogComponent, {
			data: { title, statuses, messages },
			width: '480px'
		});
	}

	// Method returns instance of MatDialog
	modalElement(title: string = '', description: string = '', waitDesciption: string = '') {
		return this.dialog.open(ConfirmEntityDialogComponent, {
			data: { title, description, waitDesciption },
			width: '480px'
		});
	}

	// Method returns instance of MatDialog
	paymentDeny(title: string = '', description: string = '', waitDesciption: string = '', user_exam_id: number) {
		return this.dialog.open(FailPaymentDialogComponent, {
			data: { title, description, waitDesciption, user_exam_id },
			width: '480px'
		});
	}

	// Method returns instance of MatDialog
	uploadSlip(title: string = '', description: string = '', waitDesciption: string = '',user_exam_id , img_path?: string) {
		return this.dialog.open(UploadSlipDialogComponent, {
			data: { title, description, waitDesciption,user_exam_id, img_path},
			width: '480px'
		});
	}
}
