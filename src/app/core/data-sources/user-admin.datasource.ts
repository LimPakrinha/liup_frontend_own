import { of } from 'rxjs';
import { catchError, finalize, tap } from 'rxjs/operators';
import { QueryParamsModel } from '../models/query-params.model';
import { BaseDataSource } from './base.datasource';
import { QueryResultsModel } from '../models/query-results.model';
import { UserAdminService } from '../../service/user-admin/user-admin.service';

export class UserAdminDataSource extends BaseDataSource {
  constructor(
    private userAdminService: UserAdminService,
  ) {
    super();
  }

  loadData(userID: number , queryParams: QueryParamsModel) {
    this.userAdminService.lastFilter$.next(queryParams);
    this.loadingSubject.next(true);
    this.userAdminService.getUserAdminById(userID, queryParams).pipe(
      tap((res: any) => {
        this.entitySubject.next(res.data.data);
        this.paginatorTotalSubject.next(res.data.total);
      }),
      catchError(err => of(new QueryResultsModel([], err))),
      finalize(() => this.loadingSubject.next(false))
    ).subscribe();
  }
}