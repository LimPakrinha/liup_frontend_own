import { BaseDataSource } from './base.datasource';
import { examService } from '../../service/exam/exam.service';

export class examDataSource extends BaseDataSource {

    constructor(
        protected examService: examService
    ) {
        super();
    }

}