import { of } from 'rxjs';
import { catchError, finalize, tap } from 'rxjs/operators';
import { QueryParamsModel } from '../models/query-params.model';
import { BaseDataSource } from './base.datasource';
import { QueryResultsModel } from '../models/query-results.model';
import { UserExamService } from '../../service/user-exam/user-exam.service';

export class UserExamDataSource extends BaseDataSource {
  constructor(
    private userExamService: UserExamService,
  ) {
    super();
  }

  loadData(queryParams: QueryParamsModel) {
    this.userExamService.lastFilter$.next(queryParams);
    this.loadingSubject.next(true);
    this.userExamService.getExamForRegister(queryParams).pipe(
      tap((res: any) => {
        this.entitySubject.next(res.data.data);
        this.paginatorTotalSubject.next(res.data.total);
      }),
      catchError(err => of(new QueryResultsModel([], err))),
      finalize(() => this.loadingSubject.next(false))
    ).subscribe();
  }
  loadExamScoreData(examID:number, queryParams: QueryParamsModel) {
    this.userExamService.lastFilter$.next(queryParams);
    this.loadingSubject.next(true);
    this.userExamService.getExamScore(examID,queryParams).pipe(
      tap((res: any) => {
        this.entitySubject.next(res.data.data);
        this.paginatorTotalSubject.next(res.data.total);
      }),
      catchError(err => of(new QueryResultsModel([], err))),
      finalize(() => this.loadingSubject.next(false))
    ).subscribe();
  }
}