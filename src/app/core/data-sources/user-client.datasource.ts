import { of } from 'rxjs';
import { catchError, finalize, tap } from 'rxjs/operators';
import { QueryParamsModel } from '../models/query-params.model';
import { BaseDataSource } from './base.datasource';
import { QueryResultsModel } from '../models/query-results.model';
import { UserClientService } from '../../service/user-client/user-client.service';

export class UserClientDataSource extends BaseDataSource {
  constructor(
    private userClientService: UserClientService,
  ) {
    super();
  }

  loadData(queryParams: QueryParamsModel) {
    this.userClientService.lastFilter$.next(queryParams);
    this.loadingSubject.next(true);
    this.userClientService.getUserClient(queryParams).pipe(
      tap((res: any) => {
        console.log("user client res")
        console.log(res)
        this.entitySubject.next(res.data.data);
        this.paginatorTotalSubject.next(res.data.total);
      }),
      catchError(err => of(new QueryResultsModel([], err))),
      finalize(() => this.loadingSubject.next(false))
    ).subscribe();
  }
}