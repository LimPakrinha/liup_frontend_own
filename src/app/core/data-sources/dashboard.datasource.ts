import { of } from 'rxjs';
import { catchError, finalize, tap } from 'rxjs/operators';
import { QueryParamsModel } from '../models/query-params.model';
import { BaseDataSource } from './base.datasource';
import { QueryResultsModel } from '../models/query-results.model';
import { examService } from '../../service/exam/exam.service';

export class DashboardDataSource extends BaseDataSource {
  constructor(
    private examService: examService,
  ) {
    super();
  }

  loadData(queryParams: QueryParamsModel) {
    this.examService.lastFilter$.next(queryParams);
    this.loadingSubject.next(true);
    this.examService.getExam(queryParams).pipe(
      tap((res: any) => {
        this.entitySubject.next(res.data.data);
        this.paginatorTotalSubject.next(res.data.total);
      }),
      catchError(err => of(new QueryResultsModel([], err))),
      finalize(() => this.loadingSubject.next(false))
    ).subscribe();
  }
}