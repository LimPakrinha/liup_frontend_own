import { of } from 'rxjs';
import { catchError, finalize, tap } from 'rxjs/operators';
import { QueryParamsModel } from '../models/query-params.model';
import { BaseDataSource } from './base.datasource';
import { QueryResultsModel } from '../models/query-results.model';
import { UserService } from '../../service/user/user.service';

export class UserDataSource{
    constructor(
        protected userService: UserService
    ) {
        
    }
}
