import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { PagesNotice } from './pages-notice.interface';

@Injectable({
	providedIn: 'root'
})
export class PagesNoticeService {
	onNoticeChanged$: BehaviorSubject<PagesNotice>;

	constructor() {
		this.onNoticeChanged$ = new BehaviorSubject(null);
	}

	setNotice(message: string, type?: string) {
		const notice: PagesNotice = {
			message: message,
			type: type
		};
		this.onNoticeChanged$.next(notice);
	}
}
