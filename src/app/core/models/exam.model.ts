import { BaseModel } from "./base.model";
export class ExamModel extends BaseModel{
	id?: number;
	name: string;
	exam_type_id: number;
	exam_type_name:string;
	exam_place: string
	exam_start_time: string
	exam_end_time: string
	exam_date: string
	register_open_date: string
	register_close_date: string
	announce_score_date: string
	cost: number;
	seat: number;
	is_active: number;
	status_seat_id:number;
	status_seat_name:string;

	clear() {
		this.name = null;
		this.exam_type_id=null;
		this.exam_type_name=null;
		this.exam_place = null;
		this.exam_date = null;
		this.exam_start_time = null;
		this.exam_end_time = null;
		this.register_open_date = null;
		this.register_close_date=null;
		this.announce_score_date=null;
		this.cost = null;
		this.seat = null;
		this.is_active = null;
		this.status_seat_id = null;
		this.status_seat_name = null;
	}
}