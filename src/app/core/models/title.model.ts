import { BaseModel } from "./base.model";

export class TitleModel extends BaseModel{
    id?: number;
	name:string;


	clear() {
		this.name = null;

	}
}