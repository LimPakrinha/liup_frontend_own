import { BaseModel } from './base.model';

export class UserExamModel extends BaseModel {
	id?: number;
	user_id: number;
	exam_id: number;
	receipt_file: string = null;
	seat_no: string;
	score: number;
	is_reg_confirmed: number;
	is_pass: number;

	clear() {
		this.user_id = 0;
		this.exam_id = 0;
		this.receipt_file = '';
		this.seat_no = '';
		this.score = 0;
		this.is_reg_confirmed = 0;
		this.is_pass = 0;
	}
}
