import {AbstractControl, ValidatorFn, FormControl, Validators} from '@angular/forms';

export class PasswordValidation {
    static MatchPassword(AC: AbstractControl) {
       let password = AC.get('password').value; // to get value in input tag
       let confirmPassword = AC.get('confirmPassword').value; // to get value in input tag
    
        if(password != confirmPassword) {
            // console.log('false');
            AC.get('confirmPassword').setErrors( {MatchPassword: true} )
            if(confirmPassword.length < 1) AC.get('confirmPassword').setErrors( {required: true} )
            // console.log(AC.get('confirmPassword').errors?.required);
        } else {
            AC.get('confirmPassword').setErrors(null);
            return null
        }
    }
}