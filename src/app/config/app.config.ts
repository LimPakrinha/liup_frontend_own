export class AppConfig {
    public static APP_NAME = 'LibArt Registration'
    public static APP_URL = 'http://localhost';
    public static API_URL = 'http://localhost/LIUP_edit/liup_backend/public/api/v1';
    public static FILEUPLOAD_URL = 'http://localhost/LIUP_edit/liup_backend/public';
    public static LIMIT_FILE_SIZE = {
        LIMIT: 5242880,
        MESSAGE: "กรุณาเลือกไฟล์ที่ขนาดไม่เกิน 5 mb"
    }
}