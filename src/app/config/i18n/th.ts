// USA
export const locale = {
	lang: 'th',
	data: {
		TRANSLATOR: {
			SELECT: 'Select your language',
		},
		MENU: {
			NEW: 'new',
			ACTIONS: 'Actions',
			CREATE_POST: 'Create New Post',
			REPORTS: 'Reports',
			APPS: 'Apps',
			DASHBOARD: 'Dashboard'
		},
		AUTH: {
			GENERAL: {
				OR: 'Or',
				SUBMIT_BUTTON: 'ยืนยัน',
				NO_ACCOUNT: 'ยังไม่มีบัญชี?',
				SIGNUP_BUTTON: 'ลงทะเบียน',
				FORGOT_BUTTON: 'Forgot Password',
				BACK_BUTTON: 'กลับ',
				PRIVACY: 'Privacy',
				LEGAL: 'Legal',
				CONTACT: 'Contact',
			},
			LOGIN: {
				TITLE: 'Login Account',
				BUTTON: 'Sign In',
			},
			FORGOT: {
				TITLE: 'Forgotten Password?',
				DESC: 'Enter your email to reset your password',
			},
			REGISTER: {
				TITLE: 'Sign Up',
				DESC: 'Enter your details to create your account',
				SUCCESS: 'บัญชีของคุณได้รับการลงทะเบียนเรียบร้อยแล้ว กรุณาใช้บัญชีที่ลงทะเบียนของคุณเพื่อเข้าสู่ระบบ'},
			INPUT: {
				EMAIL: 'อีเมล',
				TELEPHONE : 'เบอร์โทร',
				FIRSTNAME: 'ชื่อ',
				LASTNAME:'นามสกุล',
				STU_ID:'รหัสนิสิต',
				TITLE:'คำนำหน้า',
				FACULTY:'คณะ',
				DEPARTMENT: 'สาขา',
				LEVEL:'ระดับการศึกษา',
				PASSWORD: 'รหัสผ่าน',
				NATIONAL_ID:'หมายเลขประจำตัวประชาชน',
				PASSPORT:'หมายเลขPassport',
				PROVINCE:'จังหวัด',
				DISTRICT:'อำเภอ',
				SUBDISTRICT:'ตำบล',
				ZIPCODE:'รหัสไปรษณีย์',
				CONFIRM_PASSWORD: 'รหัสยืนยัน',
			},
			VALIDATION: {
				INVALID: '{{name}}ของคุณไม่ถูกต้อง',
				REQUIRED: '{{name}}ต้องการให้กรอก',
				PWD_MATCH:'{{name}}ไม่ตรงกัน',
				MIN_LENGTH: '{{name}} minimum length is {{min}}',
				AGREEMENT_REQUIRED: 'Accepting terms & conditions are required',
				NOT_FOUND: 'The requested {{name}} is not found',
				INVALID_LOGIN: '{{name}} ของคุณไม่ถูกต้อง',
				DUPLICATE: '{{name}}นีมีผู้อื่นใช้แล้ว.'
			}
		},
		ECOMMERCE: {
			COMMON: {
				SELECTED_RECORDS_COUNT: 'Selected records count: ',
				ALL: 'All',
				SUSPENDED: 'Suspended',
				ACTIVE: 'Active',
				FILTER: 'Filter',
				BY_STATUS: 'by Status',
				BY_TYPE: 'by Type',
				BUSINESS: 'Business',
				INDIVIDUAL: 'Individual',
				SEARCH: 'Search',
				IN_ALL_FIELDS: 'in all fields'
			},
			ECOMMERCE: 'eCommerce',
			CUSTOMERS: {
				CUSTOMERS: 'Customers',
				CUSTOMERS_LIST: 'Customers list',
				NEW_CUSTOMER: 'New Customer',
				DELETE_CUSTOMER_SIMPLE: {
					TITLE: 'Customer Delete',
					DESCRIPTION: 'Are you sure to permanently delete this customer?',
					WAIT_DESCRIPTION: 'Customer is deleting...',
					MESSAGE: 'Customer has been deleted'
				},
				DELETE_CUSTOMER_MULTY: {
					TITLE: 'Customers Delete',
					DESCRIPTION: 'Are you sure to permanently delete selected customers?',
					WAIT_DESCRIPTION: 'Customers are deleting...',
					MESSAGE: 'Selected customers have been deleted'
				},
				UPDATE_STATUS: {
					TITLE: 'Status has been updated for selected customers',
					MESSAGE: 'Selected customers status have successfully been updated'
				},
				EDIT: {
					UPDATE_MESSAGE: 'Customer has been updated',
					ADD_MESSAGE: 'Customer has been created'
				}
			}
		}
	}
};
