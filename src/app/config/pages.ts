import { ConfigModel } from '../core/interfaces/config';

export class PagesConfig implements ConfigModel {
	public config: any = {};

	constructor() {
		this.config = {
			'exam-data': {
				page: {
					title: 'รายการสมัครสอบ',
					desc: 'ข้อมูลการอัพเดทต่างๆล่าสุด'
				}
			},
			'user-exam-register': {
				page: { title: 'สมัครสอบ', desc: '' }
			},
			user: {
				page: { title: 'จัดการข้อมูลผู้ใช้', desc: '' }
			},
			builder: {
				page: { title: 'Layout Builder', desc: 'Layout builder' }
			},
			header: {
				actions: {
					page: { title: 'Actions', desc: 'actions example page' }
				}
			},
			profile: {
				page: { title: 'บัญชีผู้ใช้งาน', desc: '' }
			},
			404: {
				page: { title: '404 Not Found', desc: '', subheader: false }
			}
		};
	}
}
