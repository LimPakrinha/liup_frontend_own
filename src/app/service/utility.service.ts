import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../core/auth/authentication.service';
import { UtilsService } from '../core/services/utils.service';
import { inherits } from 'util';
@Injectable({
  providedIn: 'root'
})
export class UtilityService extends UtilsService{

  constructor(
    private router: Router,
    private authService: AuthenticationService,
    
  ) { 
    super();
  }

  verifyExpireToken(err) {
    if (err.error.is_expired === true) {
      localStorage.removeItem('userlogin');
      this.authService.logout(true);
      this.router.navigate(['/']);
    }
  }
}
