import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { UtilityService } from '../utility.service';

@Injectable({
  providedIn: 'root'
})
export class GenderService {
  private path = "gender"

  constructor(
    private http: HttpClient,
    private util: UtilityService,
    @Inject('API_URL') private API_URL: string,
  ) { }

  getAll() {
    return this.http.get(`${this.API_URL}/${this.path}`).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(err => {
        this.util.verifyExpireToken(err);
        return throwError(err);
      })
    );
  }
}
