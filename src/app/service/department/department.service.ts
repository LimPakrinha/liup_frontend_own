import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { UtilityService } from '../utility.service';
//import {BaseService} from '../base.service';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService{
  private path = "department"

  constructor(
    private http: HttpClient,
    private util: UtilityService,
    @Inject('API_URL') private API_URL: string,
  ) { }

  getDepartment(fid: number) {
    return this.http.get(`${this.API_URL}/${this.path}/${fid}`).pipe(
      map((res: any) => {
        return res;

      }),
      catchError(err => {
        this.util.verifyExpireToken(err);
        return throwError(err);
      })
    );
  }

  getFacultyID(did: number): Promise<any> {
    return new Promise((resolve, reject) => {
        this.http.get(`${this.API_URL}/${this.path}/${did}/faculty`)
            .toPromise()
            .then((response: any) => {
                resolve(response);
            })
            .catch((err) => {
                reject(err)
            })
    });
}
}
