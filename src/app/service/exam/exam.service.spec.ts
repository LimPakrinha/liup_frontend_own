import { TestBed } from '@angular/core/testing';

import { examService } from './exam.service';

describe('applicanttypesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: examService = TestBed.get(examService);
    expect(service).toBeTruthy();
  });
});
