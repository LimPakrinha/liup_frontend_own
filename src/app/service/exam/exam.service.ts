import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { BehaviorSubject, throwError } from 'rxjs';
import { UtilityService } from '../utility.service';
import {BaseService} from '../base.service';
import { LayoutUtilsService } from '../../core/services/layout/layout-utils.service';
import { QueryParamsModel } from '../../core/models/query-params.model';

@Injectable({
  providedIn: 'root'
})
export class examService extends BaseService{
  lastFilter$: BehaviorSubject<QueryParamsModel> = new BehaviorSubject(new QueryParamsModel({}, 'asc', '', 0, 10));
  path="exam";
  constructor(
    @Inject('API_URL') public API_URL: string,
    public http: HttpClient,
    public layoutUtilsService: LayoutUtilsService,
    public utilsService: UtilityService,
  ) {
    super(API_URL, http,  layoutUtilsService ,utilsService);
   }

   getExam(queryParams: QueryParamsModel) {
    return this.http.post(`${this.API_URL}/${this.path}/show`, queryParams).pipe(
        map((res: any) => {
            return res;
        }),
        catchError(err => {
            this.utilsService.verifyExpireToken(err);
            return throwError(err);
        })
    );
  }
 
}
