import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { UtilityService } from '../utility.service';
import {BaseService} from '../base.service';
import { LayoutUtilsService } from '../../core/services/layout/layout-utils.service';
// import { UtilsService } from '../../core/services/utils.service';
@Injectable({
  providedIn: 'root'
})
export class examTypeService extends BaseService{
  path="exam_type";
  constructor(
    @Inject('API_URL') public API_URL: string,
    public http: HttpClient,
    public layoutUtilsService: LayoutUtilsService,
    public utilsService: UtilityService,
  ) {
    super(API_URL, http,  layoutUtilsService ,utilsService);
   }
   getAll() {
    return this.http.get(`${this.API_URL}/${this.path}`).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(err => {
        this.utilsService.verifyExpireToken(err);
        return throwError(err);
      })
    );
  }

 
}
