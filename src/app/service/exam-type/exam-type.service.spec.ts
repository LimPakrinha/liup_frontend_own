import { TestBed } from '@angular/core/testing';

import { examTypeService } from './exam-type.service';

describe('applicanttypesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: examTypeService = TestBed.get(examTypeService);
    expect(service).toBeTruthy();
  });
});
