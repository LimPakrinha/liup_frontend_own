import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { throwError, BehaviorSubject, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { UserModel } from '../../core/models/user.model'
import { QueryParamsModel } from '../../core/models/query-params.model';
import { LayoutUtilsService, MessageType } from '../../core/services/layout/layout-utils.service';
import { UtilityService } from '../utility.service';

@Injectable({
    providedIn: 'root'
})
export class UserService {
    private userlogin: any;
    lastFilter$: BehaviorSubject<QueryParamsModel> = new BehaviorSubject(new QueryParamsModel({}, 'asc', '', 0, 10));
    private path = "user"

    constructor(
        private http: HttpClient,
        @Inject('API_URL') public API_URL: string,
        private layoutUtilsService: LayoutUtilsService,
        private util: UtilityService
    ) {
        this.userlogin = JSON.parse(localStorage.getItem('userlogin'));
    }

    forgotPassword(data: object) {
        return this.http.post(`${this.API_URL}/${this.path}/forgot_password`, data).pipe(
            map((res: any) => {
                return res;
            }),
            catchError(err => {
                this.util.verifyExpireToken(err);
                return throwError(err);
            })
        );
    }

    getPagination(queryParams: QueryParamsModel) {
        return this.http.post(`${this.API_URL}/${this.path}/paginate_mq`, queryParams).pipe(
            map((res: any) => {
                return res;
            }),
            catchError(err => {
                this.util.verifyExpireToken(err);
                return throwError(err);
            })
        );
    }

    getAll(filter: any) {
        let queryStr = null;

        if (filter) {
            for (const key in filter) {
                if (filter.hasOwnProperty(key)) {
                    if (filter[key]) {
                        queryStr += `${key}=${filter[key]}`;
                    }
                }
            }
        }

        return this.http.get(`${this.API_URL}/${this.path}${queryStr}`).pipe(
            map((res: any) => {
                return res;
            }),
            catchError(err => {
                this.util.verifyExpireToken(err);
                return throwError(err);
            })
        );
    }

    getUserById(userId: number) {
        return this.http.get(`${this.API_URL}/${this.path}/${userId}`).pipe(
            map((res: any) => {
                return res;
            }),
            catchError(err => {
                this.util.verifyExpireToken(err);
                return throwError(err);
            })
        );
    }

    add(data: UserModel): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.post(`${this.API_URL}/${this.path}`, data)
                .toPromise()
                .then((res: any) => {
                    (res.error) ? reject(res) : resolve(res);
                })
                .catch((error) => {
                    this.util.verifyExpireToken(error);
                    this.handleError(error)
                })
        });
    }

    update(id: number, data: UserModel): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.put(`${this.API_URL}/${this.path}/${id}`, data)
                .toPromise()
                .then((res: any) => {
                    (res.error) ? reject(res) : resolve(res);
                })
                .catch((error) => {
                    this.util.verifyExpireToken(error);
                    this.handleError(error)
                })
        });
    }

    delete(id: number): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.delete(`${this.API_URL}/${this.path}/${id}`)
                .toPromise()
                .then((res: any) => {
                    (res.error) ? reject(res) : resolve(res);
                })
                .catch((error) => {
                    this.util.verifyExpireToken(error);
                    this.handleError(error)
                })
        });
    }

    deleteUsers(ids): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.post(`${this.API_URL}/${this.path}/delete_multiple`, { id: ids })
                .toPromise()
                .then((res: any) => {
                    (res.error) ? reject(res) : resolve(res);
                })
                .catch((error) => {
                    this.util.verifyExpireToken(error);
                    this.handleError(error)
                })
        });
    }

    private handleError(error) {
        this.layoutUtilsService.showActionNotification("ระบบเกิดข้อผิดพลาด", MessageType.Read)
    }

    updatePwd(id: number, data: UserModel): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.put(`${this.API_URL}/${this.path}/${id}/pwd`, data)
                .toPromise()
                .then((res: any) => {
                    (res.error) ? reject(res) : resolve(res);
                })
                .catch((error) => {
                    this.util.verifyExpireToken(error);
                    this.handleError(error)
                })
        });
    }
}
