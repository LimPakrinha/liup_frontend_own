import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { UtilityService } from '../utility.service';

@Injectable({
    providedIn: 'root'
})
export class UserTypeService {
    private userlogin: any;
    private path = "user_type"

    constructor(
        private http: HttpClient,
        @Inject('API_URL') private API_URL: string,
        private util: UtilityService,
    ) {
        this.userlogin = JSON.parse(localStorage.getItem('userlogin'));
    }

    getAll(company_id: number) {
        return this.http.get(`${this.API_URL}/${this.path}?company_id=${company_id}`).pipe(
            map((res: any) => {
                return res;
            }),
            catchError(err => {
                this.util.verifyExpireToken(err);
                return throwError(err);
            })
        );
    }
}
