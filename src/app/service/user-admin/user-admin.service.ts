import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { UtilityService } from '../utility.service';
import { LayoutUtilsService } from '../../core/services/layout/layout-utils.service';
import {BaseService} from '../base.service';
import { BehaviorSubject, throwError } from 'rxjs';
import { QueryParamsModel } from '../../core/models/query-params.model';


@Injectable({
  providedIn: 'root'
})
export class UserAdminService extends BaseService{
  lastFilter$: BehaviorSubject<QueryParamsModel> = new BehaviorSubject(new QueryParamsModel({}, 'asc', '', 0, 10));
  public path = "user_admin"
  constructor(
    public http: HttpClient,
    public layoutUtilsService: LayoutUtilsService,
    public utilsService: UtilityService,
    @Inject('API_URL') public API_URL: string,
  ) {super(API_URL, http,  layoutUtilsService ,utilsService); }



//   getAllUser(id :number) {
//     return this.http.get(`${this.API_URL}/${this.path}/user/${id}`).pipe(
//         map((res: any) => {
//             return res;
//         }),
//         catchError(err => {
//             this.utilsService.verifyExpireToken(err);
//             return throwError(err);
//         })
//     );
// }

  getUserAdminById(userId: number, queryParams: QueryParamsModel) {
    return this.http.post(`${this.API_URL}/${this.path}/user/${userId}`, queryParams).pipe(
        map((res: any) => {
            return res;
        }),
        catchError(err => {
            this.utilsService.verifyExpireToken(err);
            return throwError(err);
        })
    );
  }

  // getUserAdmin(queryParams: QueryParamsModel) {
  //   return this.http.post(`${this.API_URL}/${this.path}`, queryParams).pipe(
  //     map((res: any) => {
  //       return res;
  //     }),
  //     catchError(err => {
  //       this.utilsService.verifyExpireToken(err);
  //       return throwError(err);
  //     })
  //   );
  // }

}
