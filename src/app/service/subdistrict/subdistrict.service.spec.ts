import { TestBed } from '@angular/core/testing';

import { SubDistrictService } from './subdistrict.service';

describe('subdistrictService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SubDistrictService = TestBed.get(SubDistrictService);
    expect(service).toBeTruthy();
  });
});
