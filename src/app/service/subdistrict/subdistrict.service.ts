import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { UtilityService } from '../utility.service';

@Injectable({
  providedIn: 'root'
})
export class SubDistrictService{
  private path = "subdistrict"

  constructor(
    private http: HttpClient,
    private util: UtilityService,
    @Inject('API_URL') private API_URL: string,
  ) { }

  getSubDistrict(did: number) {
    return this.http.get(`${this.API_URL}/${this.path}/${did}`).pipe(
      map((res: any) => {
        return res;

      }),
      catchError(err => {
        this.util.verifyExpireToken(err);
        return throwError(err);
      })
    );
  }
}
