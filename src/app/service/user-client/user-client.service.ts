import { Injectable, Inject } from '@angular/core';
import { BehaviorSubject, throwError } from 'rxjs';
import { QueryParamsModel } from '../../core/models/query-params.model';
import { UtilityService } from '../utility.service';
import { HttpClient } from '@angular/common/http';
import { LayoutUtilsService, MessageType } from '../../core/services/layout/layout-utils.service';
import { UserModel } from '../../core/models/user.model';
import { catchError, map } from 'rxjs/operators';
import {BaseService} from '../base.service';
@Injectable({
  providedIn: 'root'
})
export class UserClientService extends BaseService{
  lastFilter$: BehaviorSubject<QueryParamsModel> = new BehaviorSubject(new QueryParamsModel({}, 'asc', '', 0, 10));
  public path = "user_client"
  constructor(
    public http: HttpClient,
    @Inject('API_URL') public API_URL: string,
    private util: UtilityService,
    public layoutUtilsService: LayoutUtilsService,
  ) {
    super(API_URL, http, layoutUtilsService ,util);
  }

  getPagination(queryParams: QueryParamsModel) {
    return this.http.post(`${this.API_URL}/${this.path}/paginate_mq`, queryParams).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(err => {
        this.util.verifyExpireToken(err);
        return throwError(err);
      })
    );
  }

  getAll() {
    return this.http.get(`${this.API_URL}/${this.path}`).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(err => {
        this.util.verifyExpireToken(err);
        return throwError(err);
      })
    );
  }

  getById(id: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.API_URL}/${this.path}/${id}`).toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch(err => {
          this.util.verifyExpireToken(err);
          return throwError(err);
        })
    });
  }

  add(data: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.API_URL}/${this.path}`, data)
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          this.util.verifyExpireToken(error);
          this.handleError(error)
        })
    });
  }

  update(id: number, data: UserModel): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.put(`${this.API_URL}/${this.path}/${id}`, data)
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          this.util.verifyExpireToken(error);
          this.handleError(error)
        })
    });
  }


  getUserClient(queryParams: QueryParamsModel) {
    return this.http.post(`${this.API_URL}/${this.path}`, queryParams).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(err => {
        this.util.verifyExpireToken(err);
        return throwError(err);
      })
    );
  }
  private handleError(error) {
    this.layoutUtilsService.showActionNotification("ระบบเกิดข้อผิดพลาด", MessageType.Read)
  }

  getOwnAdminById(id: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.API_URL}/${this.path}/${id}/admin`).toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch(err => {
          this.util.verifyExpireToken(err);
          return throwError(err);
        })
    });
  }
}
