import { TestBed } from '@angular/core/testing';

import { applicanttypesService } from './applicant_types.service';

describe('applicanttypesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: applicanttypesService = TestBed.get(applicanttypesService);
    expect(service).toBeTruthy();
  });
});
