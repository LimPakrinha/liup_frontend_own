import { Inject } from '@angular/core';
import { BehaviorSubject, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map, catchError, filter } from 'rxjs/operators';
//import { LayoutUtilsService } from './core/services/layout/layout-utils.service';
import { LayoutUtilsService, MessageType } from '../core/services/layout/layout-utils.service';
import { QueryParamsModel } from '../core/models/query-params.model';
import { ExamModel } from '../core/models/exam.model';
//  import { UtilsService } from '../core/services/utils.service';
 //import { observable }   from 'rxjs/observable/';
import { UtilityService } from '../service/utility.service';

export class BaseService {

    public lastFilter$: BehaviorSubject<QueryParamsModel> = new BehaviorSubject(new QueryParamsModel({}, 'asc', '', 0, 10));
    public path: string;
    constructor(
        @Inject('API_URL') protected API_URL: string,
        protected http: HttpClient,
        protected layoutUtilsService: LayoutUtilsService,
        protected utilsService: UtilityService,
    ) { }

    list(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.API_URL}/${this.path}`)
                .toPromise()
                .then((response: any) => {
                    resolve(response);
                })
                .catch((err) => {
                    reject(err)
                })
        });
    }

    pagination(queryParams: QueryParamsModel) {
        return this.http.get(`${this.API_URL}/${this.path}?${this.utilsService.urlParam(queryParams)}`)
            .pipe(
                map((response: any) => {
                    return response;
                }),
                catchError(error => {
                    return throwError(error);
                })
            )
    }
    get(id: number): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.API_URL}/${this.path}/${id}`)
                .toPromise()
                .then((response: any) => {
                    resolve(response);
                })
                .catch((err) => {
                    reject(err)
                })
        });
    }

    getFeatures(id: number): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.API_URL}/${this.path}/${id}?include_domain=true&include_endpoint=true&include_engine=true&include_package=true&include_error_info=true`)
                .toPromise()
                .then((response: any) => {
                    resolve(response);
                })
                .catch((err) => {
                    reject(err)
                })
        });
    }

    add(data: object): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.post(`${this.API_URL}/${this.path}`, data)
                .toPromise()
                .then((response: any) => {
                    resolve(response);
                })
                .catch(err => {
                    reject(err)
                })
        });
    }

    addFeature(id: number, data: object): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.post(`${this.API_URL}/feature/${id}/${this.path}`, data)
                .toPromise()
                .then((res: any) => {
                    resolve(res);
                })
                .catch((error) => {
                    reject(error)
                })
        });
    }

    update(id: number, data: object): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.put(`${this.API_URL}/${this.path}/${id}`, data)
                .toPromise()
                .then((res: any) => {
                    resolve(res);
                })
                .catch((error) => {
                    reject(error)
                })
        });
    }
    
    delete(id: number): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.delete(`${this.API_URL}/${this.path}/${id}`)
                .toPromise()
                .then((res: any) => {
                    resolve(res);
                })
                .catch((error) => {
                    reject(error)
                })
        });
    }

    deleteFeature(id: number, data: object): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.request('delete',`${this.API_URL}/feature/${id}/${this.path}`,{body:data})
                .toPromise()
                .then((res: any) => {
                    resolve(res);
                })
                .catch((error) => {
                    reject(error)
                })
        });
    }

    deletes(ids: Array<object>): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.post(`${this.API_URL}/${this.path}/deletes`, { ids: ids })
                .toPromise()
                .then((res: any) => {
                    resolve(res);
                })
                .catch((error) => {
                    reject(error)                
                })
        });
    }

    
}
