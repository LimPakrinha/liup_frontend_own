import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { UtilityService } from '../utility.service';
import {BaseService} from '../base.service';
@Injectable({
  providedIn: 'root'
})
export class ProvinceService{
  public path = "province"

  constructor(
    @Inject('API_URL') public API_URL: string,
    public http: HttpClient,
    //public path: string,
    public util: UtilityService

  
  ) { 
  //  super(API_URL, http, 'province' ,util);
  }
  list(): Promise<any> {
    return new Promise((resolve, reject) => {
        this.http.get(`${this.API_URL}/${this.path}`)
            .toPromise()
            .then((response: any) => {
                resolve(response);
            })
            .catch((err) => {
                reject(err)
            })
    });
}
  getAll() {
    return this.http.get(`${this.API_URL}/${this.path}`).pipe(
      map((res: any) => {
        return res;

      }),
      catchError(err => {
        this.util.verifyExpireToken(err);
        return throwError(err);
      })
    );
  }
}
