import { TestBed } from '@angular/core/testing';

import { RegConfirmedService } from './reg_confirmed.service';

describe('CardTypeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RegConfirmedService = TestBed.get(RegConfirmedService);
    expect(service).toBeTruthy();
  });
});
