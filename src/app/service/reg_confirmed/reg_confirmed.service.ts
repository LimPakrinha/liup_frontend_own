import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { UtilityService } from '../utility.service';
//import {BaseService} from '../base.service';

@Injectable({
  providedIn: 'root'
})
export class RegConfirmedService{
  private path = "reg_confirmed"

  constructor(
    private http: HttpClient,
    private util: UtilityService,
    @Inject('API_URL') private API_URL: string,
  ) { }

  getAll(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.API_URL}/${this.path}`).toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch(err => {
          this.util.verifyExpireToken(err);
          return throwError(err);
        })
    });
  }
}
