import { TestBed } from '@angular/core/testing';

import { UserExamService } from './user-exam.service';

describe('UserExamService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserExamService = TestBed.get(UserExamService);
    expect(service).toBeTruthy();
  });
});
