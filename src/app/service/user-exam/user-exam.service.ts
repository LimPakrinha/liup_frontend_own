import { Injectable, Inject } from '@angular/core';
import { BehaviorSubject, throwError } from 'rxjs';
import { QueryParamsModel } from '../../core/models/query-params.model';
import { UtilityService } from '../utility.service';
import { HttpClient } from '@angular/common/http';
import { LayoutUtilsService, MessageType } from '../../core/services/layout/layout-utils.service';
import { UserExamModel } from '../../core/models/user-exam.model';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserExamService {
  lastFilter$: BehaviorSubject<QueryParamsModel> = new BehaviorSubject(new QueryParamsModel({}, 'asc', '', 0, 10));
  private path = "user_exam"
  constructor(private http: HttpClient,
    @Inject('API_URL') private API_URL: string,
    private util: UtilityService,
    private layoutUtilsService: LayoutUtilsService,
  ) {
  }

  getPagination(queryParams: QueryParamsModel) {
    return this.http.post(`${this.API_URL}/${this.path}/paginate_mq`, queryParams).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(err => {
        this.util.verifyExpireToken(err);
        return throwError(err);
      })
    );
  }

  getAll() {
    return this.http.get(`${this.API_URL}/${this.path}`).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(err => {
        this.util.verifyExpireToken(err);
        return throwError(err);
      })
    );
  }

  getById(id: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.API_URL}/${this.path}/${id}`).toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch(err => {
          this.util.verifyExpireToken(err);
          return throwError(err);
        })
    });
  }

  add(data: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.API_URL}/${this.path}`, data)
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          this.util.verifyExpireToken(error);
          this.handleError(error)
        })
    });
  }

  update(id: number, data): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.put(`${this.API_URL}/${this.path}/${id}`, data)
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          this.util.verifyExpireToken(error);
          this.handleError(error)
        })
    });
  }

  delete(id: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.delete(`${this.API_URL}/${this.path}/${id}`)
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          this.util.verifyExpireToken(error);
          this.handleError(error)
        })
    });
  }

  deletes(id: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.API_URL}/${this.path}/delete_multiple`, { id: id })
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          this.util.verifyExpireToken(error);
          this.handleError(error)
        })
    });
  }

  getExamForRegister(queryParams: QueryParamsModel) {
    return this.http.post(`${this.API_URL}/exam/user_register`, queryParams).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(err => {
        this.util.verifyExpireToken(err);
        return throwError(err);
      })
    );
  }

  getExamInfo(id: number) {
    return this.http.get(`${this.API_URL}/exam/${id}`).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(err => {
        this.util.verifyExpireToken(err);
        return throwError(err);
      })
    );
  }

  getExamUser(id: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.API_URL}/${this.path}/${id}/user`)
        .toPromise()
        .then((response: any) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err)
        })
    });
  }

  getUserExam(queryParams: QueryParamsModel){
    return this.http.post(`${this.API_URL}/${this.path}/user`, queryParams).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(err => {
        this.util.verifyExpireToken(err);
        return throwError(err);
      })
    );
  }

  getExamScore(id: number, queryParams: QueryParamsModel) {
    return this.http.post(`${this.API_URL}/${this.path}/score/${id}`, queryParams).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(err => {
        this.util.verifyExpireToken(err);
        return throwError(err);
      })
    );
  }

  getExamScoreDetail(id: number) {
    return this.http.get(`${this.API_URL}/${this.path}/detail/${id}`).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(err => {
        this.util.verifyExpireToken(err);
        return throwError(err);
      })
    );
  }

  private handleError(error) {
    this.layoutUtilsService.showActionNotification("ระบบเกิดข้อผิดพลาด", MessageType.Read)
  }
}
